# LEDGER  Work Plan ~ CoBox

## Index
1. [Title and Abstract](#1-cobox-for-a-peer-to-peer-cooperative-cloud)
2. [Objectives and Value Proposition for the MVP (Minimum Viable Product)](#2-objectives-and-value-proposition-for-the-mvp)
3. [Workplan](#3-workplan)
  + 3.1. [Resources (list all members of team with functions + extra or subcontractors)](#31-resources)
  + 3.2. [Architecture schematics overview](#32-architecture-schematics-overview)
  + 3.3. [Research plan (what we still need to know and research questions)](#33-research-plan)
  + 3.4. [Tasks breakdown and Gantt (when what who how much time etc.)](#34-task-breakdown-and-gantt)
  + 3.5. [Business plan layout](#35-business-plan-layout)
4. [Implementation and test](#4-implementation-and-test)
5. [Delivery](#5-delivery)
6. [Evaluation Methodology](#6-evaluation-methodology)
7. [Budget](#7-budget)
8. [Appendix](#8-appendix-interview-questions-for-needfinding-mission)

## 1. CoBox for a peer-to-peer cooperative cloud

### Abstract:

Coops often rely on proprietary services for managing their data, which do not reflect their cooperative principles or business practices.

CoBox is an innovative suite of open hardware and software providing accessible governance tools for organisations and networks. It comprises a human-centric plug and play server with pre-installed privacy enhancing coop software designed to promote members' data sovereignty. CoBox seeks to build on the historic tendency for networks of coops to collaborate.

CoBox brings a cooperative approach to hosting, treating data as a common good owned by citizens. Leveraging the benefits of self-hosting combined with peer-to-peer technologies to share responsibilities of maintaining data availability.

As well as serving internal organisational governance needs these simple to use tools provide the infrastructure for governance of the CoBox network, to manage, research and guide the ecosystem, providing a state of the art alternative to corporate models.

## 2. Objectives and Value proposition for the MVP

### Problem and Solution

#### For Society
* **Problem**: Societal loss of data sovereignty to corporations.
* **Solution**: CoBox brings a cooperative approach to hosting, treating data as a common good owned by citizens. Providing infrastructure to support co-operative sector and beyond to regain data sovereignty.

#### For Technological Ecosystem
* **Problem**: Over dependence on centralised systems, owned by corporations which require an internet connection.
* **Solution**: Offering accessible software and hardware which can support offline first tools for governing and hosting data (as a commons) with the ease of use of corporate software.

#### For Co-operatives
* **Problem**: Despite extensive global networks of coops there is no equivalent cooperative network for data management. Over reliance on corporate tools which do not match cooperative principles.
* **Solution**: CoBox seeks to build on the historic tendency for networks of coops to collaborate. CoBox leverages advances in modern P2P technologies which enable a technical infrastructure that maps to existing cooperative principles and patterns of governance.

## 3. Workplan
### 3.1 Resources

* _Business Developer_: Mooness Davarian
* _Software Engineer_: Kieran Gibb
* _Software Engineer_: Dan Hassan
* _Researcher_: Jaya Klara Brekke
* _Software Engineer_: Greg Jones
* _Researcher_: Sophea Lerner


### 3.2 Architecture schematics overview

* [MVP Design Document](mvp/mvp-design.md)

### 3.3 Research plan

* [Needfinding Plan](research/CoBox_Needfinding.md)

#### 3.3.1 Aims and objectives

**Aim**
To discover user-needs to guide the MVP design for a distributed cloud infrastructure.

**Objectives**
To understand the extent to which people are unhappy with corporate cloud tools and why exactly

To decide whether to go with p2p or supernode (server/client office with p2p backup.)

To understand how sysadmin is done within an organisation and where CoBox fits within this.

**Desired outcomes of the research**

To gather enough information to inform a viable MVP

#### 3.3.2 Research Timeline
***July:***
- Define MVP
- Prepare and conduct needfinding interviews (see appendix for interview questions)

***August:***
- Write needfinding report to feed into second iteration of MVP

***September***
- Conduct first peer-design session with Group 1
Aim: Test assumptions and solutions coming out of the needfinding and refine MVP and development plan accordingly.
- Incorporate findings into third iteration of MVP

***November***
- State-of-the-art research on platform coop governance and business models.
Research questions:
    1. What are the different types of business and organisational models of platform cooperatives?
    2. What business and organisational models are emerging as part of decentralised, networked systems, DLT, blockchain, or otherwise?
    3. What types of economic thinking underpin these?
    4. Which might represent long-term alternatives to data-extractive and VC models?
    5. Are there tools and infrastructures currently lacking to make these organisational models more feasible?
    6. What other challenges are there, eg. legal and economic to make these feasible?

Aim: the report is highlevel and can feed into both the future business model of CoBox, but also feed into governance needs of cooperatives using CoBox - ie. further ideas research for the device itself.
- Incorporate findings into fourth iteration of MVP
- Incorporate findings into CoBox business plan

***December***
- Conduct second peer-design session with Group 2
Aim: Present first iteration of working code, test prototype and refine the MVP and development plan accordingly
- Incorporate into fifth iteration of MVP

***January***
- Conduct third peer-design session with Group 1
Aim: Test near-complete product with first group. Focus on UX.
- Incorporate into final iteration of MVP

***March***
- Conduct presentation of CoBox and final round of feedback with a mix of people from groups 1 and 2 and other interested cooperatives.
Aim: present and test the final CoBox with lead participants to get last round of feedback, ideas and where to go from here before Ledger funding finishes.

#### 3.3.3 Key topics to cover in the needfinding research
* List 5-10 specific topics or questions, for our own purposes that need to be answered with this research

1. Why are people not happy with corporate tools? What does alternative/ ethical/ politically sound tools mean to them?
2. pure p2p or server/client office with p2p backup
3. what volumes of data do organisations need to backup
4. how often to backups need to happen, ideally
5. how do organisations currently manage sysadmin --> would they want to distribute this / have a GUI that requires no sysadmin role
6. what collective governance tools do they currently use
7. how widely differentiated are access permissions within the organisation
8. do they handle user-data that would need to comply with GDPR --> if yes, how much and what
9. what kinds of organisations would the technology be well suited for, and what kinds of organisations would have different needs


#### 3.3.4 Meta themes**
*Larger issues to keep in mind when interviewing and observing participants:*

1. data privacy / anonymity
2. What does "alternatives" to corporate tools actually mean? FLOSS?
Non-data extractive? Non-Silicon valley? What are cooperatives and people's principles and politics about this.
3. key management issues


#### 3.3.5 Approach and activities
* How are you defining the research activities to get to the goals? This is an overview of the different types of research activities, including number and type of participants, locations, etc. This may be primary research, secondary, etc.

#### 3.3.6 Team and roles
* Who’s going to conduct the fieldwork, no. of people in-field, etc. and what are the roles in-field (i.e. leading, taking notes, video..)



| Who | What | With |
| -------- | -------- | -------- |
| Jaya Brekke  | Conducts interview  | MayDay Rooms rep1  |
| Jaya Brekke  | Conducts interview  | MayDay Rooms rep2  |
| Jaya Brekke  | Conducts interview  | Norla organisation rep1|
| Jaya Brekke  | Conducts interview  | Norla organisation rep1|
| Mooness Davarian| Conducts interview | Outlandish rep1      |
| Mooness Davarian| Conducts interview | Outlandish rep2      |
| Mooness Davarian| Conducts interview | CoTech/Animorph rep1 |
| Mooness Davarian| Conducts interview | CoTech/Animorph rep2 |
| Sophea   | Reviews interview transcrips| Jaya and Mooness|


### 3.4 Task breakdown and Gantt

* [Task breakdown, budget & Gantt](assets/cobox-gantt-chart-w-budget.pdf)

### 3.5 Business plan layout

#### 3.5.1 What do we mean by business success?

> Human Centric puts purpose before profit, puts people before profit, that's what we mean with it Human Centric means that what we are developing first and foremost useful for the society and then it is sustainable, of course, you can make some money out of it, you can sustain and grow your organisation but it has to benefit the society around you, the people I will say even the living beings, plants and animals included.
>
_**Jaromil, CTO of Dyne.org**_ https://vimeo.com/340937562

The LEDGER programme's tagline is 'The Venture Builder for Human Centric Solutions'. Success is defined as assisting the ventures become sustainable in a way which also gives back to society. "Business Success" is defined as:

1.  benefiting society
2.  benefiting cooperatives and SME ecosystem
3.  establishing a sustainable business

Jessica Gordon Nembhard argues co-operatives benefit society (2014). CoBox seeks to create a sustainable digital service co-operative which researches and develops new business models which utilise the technologies we produce.

As part of the Ledger grant CoBox will fund state of the art research on new business models P2P technologies afford coops and secondary coops.

#### 3.5.2 Market
* Europe has 170,000+ cooperatives with an annual turn-over of more than 1000bn Euro
* Many are already organised into sector-specific networks
* Networks have existing conferences, events and communications channels.

#### 3.5.3 Year 4 Projection
* Sales: 600 boxes sold @ 200 euro each
* CoBox users: 500 coops engaged in the network
* SaaS Clients: 200 SaaS contracts
    * 100 small orgs (490e/yr each)
    * 70 medium orgs (1490e/yr each)
    * 30 large orgs (4490e/yr each)
* Hashbase option: 200 coops @ 7e/month each
* CoBox Federation Members: 50 coops @ 100e/yr
* Profit 354,400e
* Employees: 10

#### 3.5.4 How will we achieve this?

CoBox will itself be constituted as a digital coop with two primary income streams: sales of the physical CoBox and a SaaS model for provision of training, customisation and features.

Drawing on Ostrom (1990) and Benkler (2006), CoBox will accumulate shared gains for the ecosystem by offering a hashbase alternative for search and discovery, collectively governed as a commons service by its stakeholders.

Basic membership dues will support operational costs for the network governance structure.

#### 3.5.5 Initial Strategy

Our initial strategy is to focus on a subset of our target market (co-operatives). This subset are Workers Coops and other motivated small and medium co-operative organisations who have expressed desire to move away from the corporate cloud.

CoBox is state of the art and cutting edge so it's integral that we have experimental organisations willing to walk with us through the turbulent early stages of research and development.

Once a baseline of software and hardware has been established with evidenced usefulness for our early adopters we can then be agile in the directions which need to be taken to take us to each subsequent round of growth.

#### 3.5.6  Lean needs

The CoBox founding team consists of new parents and community activists. This adds frictions around coordination but overall in our experience over the last year lays the foundation for a potentially more resilient organisation which is more accessible to those from our ecosystem with accessibility barriers in their myriad forms.

However, being embedded in progressive co-living communities means that our material needs are less than many organisations. If we acheive 30k EUR per month, then this is plenty for a robust team of 10 people working part time with this project as their primary commerical focus. Sustainability happens at half this figure.

#### 3.5.7 Principles

CoBox is yet to establish the core principles but one of our founding principles is

* Peers, not Users

As such our business plan will emerge alongside our peer research as we truly understand from our peers what their needs are and how our coop, products and research can best serve the wider European and global co-operative sector.

#### 3.5.8 Business Timeline
Please see section [3.3.2 Research Timeline](#332-research-timeline) for a complete view. The Gantt chart should be considered the canonical source of truth for Business deliverable timelines.

In addition to the following timeline we will be concurrently following the timeline suggested by our business mentor from Blumorpho.

* [Blumorpho LEDGER Work Plan with CoBox](assets/blumorpho-ledger-work-plan-cobox.pdf)

***July:***
- Identify hardware
- Gather supporting research

***August:***
- Order hardware
- Comparative analysis with similar hardware offers
- Start mapping distribution pipeline for hardware prototypes

***September***
- Incorporate research findings into business communications with early partners

***November***
- State-of-the-art research on platform coop governance and business models.
- Incorporate findings into fourth iteration of MVP
- Incorporate findings into CoBox business plan

***December***
- Conduct second peer-design session with Group 2
- Incorporate into fifth iteration of MVP
- Incorporate findings into CoBox business plan

***January***
- Conduct third peer-design session with Group 1
- Incorporate into final iteration of MVP
- Incorporate findings into CoBox business plan

***February***
- Finalise prototypes

***March***
- Conduct presentation of CoBox and final round of feedback with a mix of people from groups 1 and 2 and other interested cooperatives.
- Present CoBox business plan

## 4. Implementation and test

* [MVP Implementation and Testing](mvp/implementation-and-test.md)

## 5. Delivery

As a pre-launch venture CoBox's primary deliverables orbit around Research, MVP Development and Market Readiness (Business Plan) which will feed into the launch of our MVP.

Both the research plan and business plan correspond to modular deliverables. Tech development deliverables have been broken into sub-components to allow for iterative feedback from both the business research and peer needfinding research.

* [Research Deliverables](#332-research-timeline)
* [MVP Development Deliverables](mvp/tech-deliverables.md)
* [Business Deliverables](#358-business-timeline)

## 6. Evaluation methodology

Like the cooperatives that we aim to serve, CoBox will not be a solely profit-driven enterprise. Our proposed measurement metrics have therefore been drawn and adapted from those of other open source, p2p and cooperative community projects.

1. Measuring peer-engagement and network growth will comprise three sub-metrics:
    + i. Increase in size of network
    + ii. Increase in internal connectivity
    + iii. Increase in connections to valuable third parties
2. Measuring share ratio will allow peers to recognise their own contributions towards maintaining key functions of the network (data availability, resilience and backup).
3. Happiness-unhappiness continuum, drawn from 'Network Weaving', will allow us to measure on a qualitative scale the health of peer relationships.
4. Transparency of governance will allow us to evaluate the CoBox team's responsiveness and commitment to the networked cloud.
5. Finally, we will measure profit, towards sustainable and self-sustaining growth.

At this early stage which are startup phase we will be focusing on (1) and (4) through blog posts, twitter feed, public communications, healthy community relations with the tech groups whose work we’re building on.

Once we have the MVP and have launched we will then be adding the rest of the metrics.


## 7. Budget

This is included on the Gantt chart.

* [Task breakdown, budget & Gantt](assets/cobox-gantt-chart-w-budget.pdf)


## 8. Appendix: Interview questions for needfinding mission

| General interview questions | Questions | This will help us to know... |
| -------- | -------- | -------- |
|Introduction|We are interested to know about your experiences with filemanagement and backup systems. There are no right or wrong answers. |Need an introduction that avoids people trying to say the "right" things about non-corporate solutions and speak of their experiences.
| Kick-off     | Please describe your organisation and your role in it?|the role of the interviewee in their organisation in their own words|
|Kick-off| How many people are there in your organisation? What other roles are there in your organisation? And what are their responsibilities?    | Helps define the different people "types" that CoBox needs to serve     |
|Grand tour|Can you describe a working day for me?|the workings of the person in the organisation, what ways decisions and tasks are done and through what processes|
|Grand tour | I want to ask about the governance tools that you use in your organisation - (nts: mention here that we're not interested in judging the choices, but in understanding them!) - so for example do you use any collective decision-making tools? If yes, what? (note to self: e.g. loomio) |to know what tools they use and to what extent digital tools are used vs in-person meetings|
|Grand tour | Do you use any shared calendar tools? If yes, what? (nts: e.g. thunderbird, google, ical)|how the organisation collaborates, how much of that is facilitated by digital tools or not|
|Grand tour | Do you use any meeting tools? If yes, what? (nts: e.g. mumble, google talk)|how the organisation collaborates, how much of that is facilitated by digital tools or not|
|Grand tour |Do you use any shared/collaborative budgeting tools?  If yes, what? (nts: e.g. cobudget) ||
|Grand tour | Do you use any collaborative document editing tools? If yes, what? (nts: e.g. etherpad/google docs)||
|Grand tour |Do you use any shared file storage / file management tools? If yes, what? (nts: e.g. google drive / next cloud / own cloud) |self-hosted or cloud based, corporate or not|
|Grand tour |How do you deal with email hosting? (nts: e.g. gmail, self-hosting etc)|level of knowledge and experiences of self-hosting and whether it is a priority|
|Grand tour |Do you all use a particular / the same email client or web interface for email?|how coherent the organisation is in its choices of tools or how much it is a patchwork built up over time. This will tell us also about the possible processes we need to be aware of for adoption.|
|Grand tour |What do you do for website hosting? (e.g. self-hosted or corporate or?)|the level of technical resources and ability of the organisation|
|Grand tour|What filemanagement tools does your cooperative currently use? (google drive or dropbox or other)|whether the organisation uses self-hosted or cloud based, corporate or not|
|Follow up|What is important to you about these tools? (for each one?)|To find out the priorities for people, whether it is their usability, that they allow shared access, availability, that they do not cost money to use etc. |
|Grand tour  | Is there anything you particularly like or dislike about them?    |whether there are repeated problems or annoyances|
|Grand tour|Can you describe any memorable good or bad experiences with your existing tools? What happened?|When and how existing tools majorly fail people|
|History|When did you start using this/these particular file management tool(s)?|how old the organisations systems are, how easily they adopt new ones|
| Grand tour    | How do you decide what software / systems to use?    |What are the organisation's priorities (cost, convenience, principles etc..)|
|Follow up|What mattered in the decisions about these tools?|Pragmatics versus principles and ethics in the decisions|
|Follow up|What, if anything, bothers you about corporate cloud based tools?|Find out the details whether it is the commercialisation of data, privacy or other things that matter to people. Watch out for virtue signalling and the differences between what people say and do.|
|Follow up|What does "alternatives" to corporate tools mean to you? (for example, FLOSS or not? Privacy / selling of data)||
|Grand tour|What would be the ideal characteristics of a filemanagement system?|Priorities and main needs from the persons perspective|
|Grand tour|What would be the ideal characteristics of a backup system?|Priorities and main needs from the persons perspective|
| Specific    | How do you manage sysadmin as a coop?    | In case 'sysadmin' hasn't been identified (it's often relatively invisible) this helps draw it out    |
|Specifics | What kinds of files or data do you store | to find out what the volume, type and freuency of data backup needs |
|Specifics | Do you use encryption for anything at all in your organisation? ||
|Follow up | If yes - for what, and why? ||
|Follow up | If yes - how do you manage keys? (passwords, backup, multi-device)||
|Specifics | How many devices do you use to work on?    |
|Specifics   | (if more than one) Do you experience any frustrations with this?  If yes, can you specify what they are?  ||
|Follow up   | What things do you like about these tools / what would you like to see improved?    |
|Specifics|Do you back-up your files (personally and/or as an organisation)? If so, how?||
|Specifics    | Has your organisation ever conducted a risk assessment for data-loss / do you have any procedures in place for what to do in the case of data loss?    ||
|Specifics | Are there any files or data that your organisation has to which either read or write access is restricted to specific individuals?||
|Follow-up | If yes, what kind of data is it? How do you manage that? (nts: e.g. password protected file; files not visible to certain user logins; etc) ||
|Follow-up | Have you had any problems with that way of managing access and permissions? ||
|Tentative proposal testing |When I say the words: backing up amongst a network of trusted peers, relying on your peers for back-up, your peers have your back (up) - what does that mean to you?|To find out expectations, and doubts, example from a tech person "you are crazy, how would the network deal with bandwidth"|



|FLOSS enthusiast/ expert:|Question| This will help us know|
| -------- | -------- | -------- |
|Direct question to get their experienced opinions|Why do you think people do not use alternatives to corporate tools, like nextcloud, keybase etc?| Their experiences of why people do not use existing alternatives.|
|Direct question to get their experienced opinions|What do you think we need to know?|Existing tools, common mistakes, good ideas|
||||
