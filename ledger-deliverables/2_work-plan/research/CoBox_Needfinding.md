# CoBox Needfinding

## Resources:

* [Standford Software Design Experiences: Needfinding Crib Sheet](https://hci.stanford.edu/courses/cs447/docs/NeedFindingCribSheet.pdf)
* [SecondMuse Needfinding Framework (9 Chapter Index)](http://internetfreedom.secondmuse.com/needfinding/)
    * [Developing a Research Plan](http://internetfreedom.secondmuse.com/framework-elements/developing-a-research-plan/)
    * [Establishing Goals and Research Questions](http://internetfreedom.secondmuse.com/framework-elements/establishing-goals-and-research-questions/)
    * [Conducting Interviews](http://internetfreedom.secondmuse.com/framework-elements/conducting-interviews/)
* [DeltaChat Needfinding Report](https://delta.chat/assets/blog/dcneedfindingreport.pdf)

**Current MVP**
Blind replication amongst peers. A peer-to-peer back-up system with each CoBox containing pre-installed OS with non-corporate organisational tools. 

**High-level descriptions of CoBox:**
- A peer-to-peer back-up system for organisations using or wanting to use non-corporate tools
- A peer-to-peer cooperative cloud
- A kind of decentralized and privacy aware version of dropbox
- A tool for networks of organisations to help each other with back-ups and file recovery
- A box with a suite of non-corporate, privacy aware organisational tools, that shares the back-up load with other similarly minded organisations


## Attitude:
The most important thing about need finding in design thinking is that we look without presupposing what we are looking for.  We trust that our ability to define the problem will emerge during the need finding process. 

## Research Plan:

**GOALS & OUTCOMES:**

* ***Why are we conducting this research?***

To discover user-needs to guide the MVP design for a distributed cloud infrastructure.

* ***What do we want to get out of it specifically?*** 

To understand the extent to which people are unhappy with corporate cloud tools and why exactly

To decide whether to go with p2p or supernode (server/client office with p2p backup.)

> [name=peg] we need to come up with terms to describe these two proposals which is consistent accross our different docs.  i've been calling it 'strong and weak p2p' which i dont like at all. 
> [name=jaya] how bout p2p vs supernode model?
> [name=kieran] +1


To understand how sysadmin is done within an organisation and where CoBox fits within this.

> [name=peg] if we are building something to appeal to organisations who dont have a sysadmin person (and are therefore likely to use commercial cloud services) we need to establish that european cooperatives fit this 
> [name=jaya]yep, so we have added specific sysadmin questions in the table with qustions below

* What are the desired outcomes?

To gather enough information to inform a viable MVP

**KEY RESEARCH QUESTIONS & TOPICS:**
* List 5-10 specific topics or questions, for our own purposes that need to be answered with this research

1. Why are people not happy with corporate tools? What does alternative/ ethical/ politically sound tools mean to them?
2. pure p2p or server/client office with p2p backup
3. what volumes of data do organisations need to backup
4. how often to backups need to happen, ideally
5. how do organisations currently manage sysadmin --> would they want to distribute this / have a GUI that requires no sysadmin role
6. what collective governance tools do they currently use
7. how widely differentiated are access permissions within the organisation
8. do they handle user-data that would need to comply with GDPR --> if yes, how much and what
9. what kinds of organisations would the technology be well suited for, and what kinds of organisations would have different needs
> [name=kieran] have added above as it may be supernode vs p2p are applicable for different contexts

> [name=peg] as well as /as part of q3 we could say 'what involvement from members is required to make backups' if we are appealing to organisations who dont have a sysadmin, it needs to be automated and something people just dont have to think about.
> 
> what collaboration tools do people currently use? -this relates to the extent to which we need to put energy into developing conflict resolution 
> [name=jaya] yep - we got that in the specific table of questions below.

**META THEMES:**
* As you interview and observe participants keep in mind these larger issues you are exploring

1. data privacy / anonymity
2. What does "alternatives" to corporate tools actually mean? FLOSS?
Non-data extractive? Non-Silicon valley? What are cooperatives and people's principles and politics about this.
3. key management issues




**APPROACH & ACTIVITIES:**
* How are you defining the research activities to get to the goals? This is an overview of the different types of research activities, including number and type of participants, locations, etc. This may be primary research, secondary, etc.

**SELECTION CRITERIA:** 
* Who are you studying and why? This section explains how you selected research location, participants, etc.

CoTech, NORLA and some Australian groups. 

> [name=kieran] and possibly two london-based co-operatives (Agorama Server Co-op and Strike! magazine have both expressed keen interest)

**TEAM & ROLES:**
* Who’s going to conduct the fieldwork, no. of people in-field, etc. 

Jaya, Mu, Sophea

* What are the roles in-field (i.e. leading, taking notes, video..)

**DRAFT SCHEDULE:** 
* This is a general sense of how the research may go and will be refined as your research begins. Depending on the level of research this may include: 
    * team meetings and debriefs:
        * meeting with Sophea to review & refine needfinding questions/process
        * meeting in mid-August to compile interview results
    * travel time: N/A
    * participant research slots:
        * each interviewer to organise their own interviews (how many?)
            * Jaya: NORLA interviews
            * Mu: CoTech interviews
            * Sophea: Aus groups?

**LOGISTICS:**
* This includes travel information, materials needed, contact information of other people participating in the research like an in-country guide, etc.






## Interview questions:

| General interview questions | Questions | This will help us to know... |
| -------- | -------- | -------- |
|Introduction|We are interested to know about your experiences with filemanagement and backup systems. There are no right or wrong answers. |Need an introduction that avoids people trying to say the "right" things about non-corporate solutions and speak of their experiences.
| Kick-off     | Please describe your organisation and your role in it?|the role of the interviewee in their organisation in their own words|
|Kick-off| How many people are there in your organisation? What other roles are there in your organisation? And what are their responsibilities?    | Helps define the different people "types" that CoBox needs to serve     |
|Grand tour|Can you describe a working day for me?|the workings of the person in the organisation, what ways decisions and tasks are done and through what processes|
|Grand tour | I want to ask about the governance tools that you use in your organisation - (nts: mention here that we're not interested in judging the choices, but in understanding them!) - so for example do you use any collective decision-making tools? If yes, what? (note to self: e.g. loomio) |to know what tools they use and to what extent digital tools are used vs in-person meetings|
|Grand tour | Do you use any shared calendar tools? If yes, what? (nts: e.g. thunderbird, google, ical)|how the organisation collaborates, how much of that is facilitated by digital tools or not|
|Grand tour | Do you use any meeting tools? If yes, what? (nts: e.g. mumble, google talk)|how the organisation collaborates, how much of that is facilitated by digital tools or not|
|Grand tour |Do you use any shared/collaborative budgeting tools?  If yes, what? (nts: e.g. cobudget) ||
|Grand tour | Do you use any collaborative document editing tools? If yes, what? (nts: e.g. etherpad/google docs)||
|Grand tour |Do you use any shared file storage / file management tools? If yes, what? (nts: e.g. google drive / next cloud / own cloud) |self-hosted or cloud based, corporate or not|
|Grand tour |How do you deal with email hosting? (nts: e.g. gmail, self-hosting etc)|level of knowledge and experiences of self-hosting and whether it is a priority|
|Grand tour |Do you all use a particular / the same email client or web interface for email?|how coherent the organisation is in its choices of tools or how much it is a patchwork built up over time. This will tell us also about the possible processes we need to be aware of for adoption.|
|Grand tour |What do you do for website hosting? (e.g. self-hosted or corporate or?)|the level of technical resources and ability of the organisation|
|Grand tour|What filemanagement tools does your cooperative currently use? (google drive or dropbox or other)|whether the organisation uses self-hosted or cloud based, corporate or not|
|Follow up|What is important to you about these tools? (for each one?)|To find out the priorities for people, whether it is their usability, that they allow shared access, availability, that they do not cost money to use etc. |
|Grand tour  | Is there anything you particularly like or dislike about them?    |whether there are repeated problems or annoyances|
|Grand tour|Can you describe any memorable good or bad experiences with your existing tools? What happened?|When and how existing tools majorly fail people|
|History|When did you start using this/these particular file management tool(s)?|how old the organisations systems are, how easily they adopt new ones|
| Grand tour    | How do you decide what software / systems to use?    |What are the organisation's priorities (cost, convenience, principles etc..)| 
|Follow up|What mattered in the decisions about these tools?|Pragmatics versus principles and ethics in the decisions|
|Follow up|What, if anything, bothers you about corporate cloud based tools?|Find out the details whether it is the commercialisation of data, privacy or other things that matter to people. Watch out for virtue signalling and the differences between what people say and do.|
|Follow up|What does "alternatives" to corporate tools mean to you? (for example, FLOSS or not? Privacy / selling of data)||
|Grand tour|What would be the ideal characteristics of a filemanagement system?|Priorities and main needs from the persons perspective|
|Grand tour|What would be the ideal characteristics of a backup system?|Priorities and main needs from the persons perspective|
| Specific    | How do you manage sysadmin as a coop?    | In case 'sysadmin' hasn't been identified (it's often relatively invisible) this helps draw it out    |
|Specifics | What kinds of files or data do you store | to find out what the volume, type and freuency of data backup needs |
|Specifics | Do you use encryption for anything at all in your organisation? ||
|Follow up | If yes - for what, and why? ||
|Follow up | If yes - how do you manage keys? (passwords, backup, multi-device)||
|Specifics | How many devices do you use to work on?    |
|Specifics   | (if more than one) Do you experience any frustrations with this?  If yes, can you specify what they are?  ||
|Follow up   | What things do you like about these tools / what would you like to see improved?    |
|Specifics|Do you back-up your files (personally and/or as an organisation)? If so, how?||
|Specifics    | Has your organisation ever conducted a risk assessment for data-loss / do you have any procedures in place for what to do in the case of data loss?    ||
|Specifics | Are there any files or data that your organisation has to which either read or write access is restricted to specific individuals?||
|Follow-up | If yes, what kind of data is it? How do you manage that? (nts: e.g. password protected file; files not visible to certain user logins; etc) ||
|Follow-up | Have you had any problems with that way of managing access and permissions? ||
|Tentative proposal testing |When I say the words: backing up amongst a network of trusted peers, relying on your peers for back-up, your peers have your back (up) - what does that mean to you?|To find out expectations, and doubts, example from a tech person "you are crazy, how would the network deal with bandwidth"|



|FLOSS enthusiast/ expert:|Question| This will help us know|
| -------- | -------- | -------- |
|Direct question to get their experienced opinions|Why do you think people do not use alternatives to corporate tools, like nextcloud, keybase etc?| Their experiences of why people do not use existing alternatives.|
|Direct question to get their experienced opinions|What do you think we need to know?|Existing tools, common mistakes, good ideas|
||||

---


## Possible interview methods

*Source: https://hci.stanford.edu/courses/cs447/docs/NeedFindingCribSheet.pdf*

**Lead user interviews** 
What:  Interviews with fanatics (the most rabid consumers of an experience) 
When:  You would like to see the future of usage, or understand an experience from the perspective of its most critical subject. 
How: Beyond traditional interview structure, you should 
* Look for the most extreme users (use your network, look for blog owners, etc. to find people like the guy who is visiting every Starbucks in the world) 
* Make contact, and state your interest in them and their views
* Ask questions that are more open ended and blunt “Why do you think people pay more for coffee now than they did in the past?”  “What do you think we need to know?” 
* Engage them in an ongoing way – lead users make great testers for your prototypes! 

**Expert interviews** 
What:  Interviews with subjects who have domain expertise 
When:  You need to come up to speed on the context of your design quickly 
How:  Beyond traditional interview structure, you should 
* Ask open ended questions that allow them to educate you.  “I am a designer, so I don’t really understand coffee chemistry.  As a food chemist, can you explain to me how coffee ‘works’?” 
* Try to understand their role in the user’s experience.  “If I come in to your store to get a coffee, what, as the barista, do you do?” 
* Gather ideas for needs that experts may uniquely see.  e.g.  “What bothers you most about you job?”

**Surveys**
What:  A series of carefully structured questions that can be  
When: You would like to get many perspectives rapidly or quantify / test insights 
How: Steps 
* Identify screener for subjects 
* Develop and sequence questions (see above rules) 
* Include explanation of survey in survey instructions 
* Provide progress updates through survey 
* Ask at least one open ended question 
* Test the survey before releasing it •www.surveymonkey.com and other tools are very useful 

**History interview** 
What:  An interview intended to understand a sequence of events 
When:  You would like to understand the historical context, or where subject behavior, feelings, or thoughts came from 
How:  After traditional early interview elements 
* Establish subject - “What is your favorite beverage?” 
* Signal interest – “How long has it been your favorite?  Why?” 
* Establish start – “What is your earliest memory of [beverage]?” 
* Walk forward – “What happened next?  Next? Next?” 

**Process mapping** 
What: A tool to understand the COMPLETE experience 
When:   
How:  Start with the experience you are considering (e.g. buying coffee) 
* From the user’s point of view, mentally step back to the earliest stage of the process (e.g. thinking  about coffee, or waking up, etc.) 
* Step through every element of the process, recording it •Analyze your map: what happens at each of these stages?  Why?  How do the stages relate?  What opportunities do you see? 

**Laddering (or 5-why) interview** 
What:  A method for driving to implicit causes based on asking why multiple times 
When:   
How:  After traditional early interview elements 
* Warn of your weird behavior:  “Have you ever talked to a five year old?  I am going to act like one and ask you why again and again.  Even when you give a good answer, I will continue to ask you why.  My goal is to understand why you do what you do at the deepest possible level.” 
* Focus on something important to the subject (where there actually is a why) 
* Establish subject - “What is your favorite beverage?” 
* Establish base interest – “What are three reasons why it is your favorite?” 
* Dive into each reason – “You said [reason one] why is that important to you?” 
* “Why?” 
* “Why?”
* This is a difficult interview technique as subjects can rarely articulate why they do what they do – in fact, they are often unaware of their own motives 
* Allow space and silence for subjects to answer the hard why questions 
* NOTE:  This technique can be used in a variety of ways.  Toyota uses a process like this uncover the root causes of manufacturing problems.

**Cultural context interview** 
What:  An unfocused tool for understanding 
When:  You would like to formulate implicit needs based in part on the more general values of a subject 
How:  This interviews requires a good amount of trust, so you may use this tool at the end of another type of interview.  This interview is relatively unstructured, but there is a list of questions that we have found to be illuminating.  Note the need to sequence the questions. 
* Start with a statement of intent – “I’d like to get a sense of who you are and what you care about.” 
* Where did you grow up? 
* How did you like that? 
* What was our family like? 
* What are your favorite (books, movies, TV shows)?  Why? 
* Do you have any hobbies? What are they? 
* What is most important to you? 
* What do you wish for? 
* What keeps you up at night? 
* What are your fears? 
* Note that the magic is in the follow up questions.  Everywhere your subject displays enthusiasm, give them room to run.  Ask lots of follow up questions, and go deep into the drivers of their enthusiasm. 
* Analyze your findings – the seemingly disjointed pieces you gather in this interview can be combined with insight from other tools to form a great story.