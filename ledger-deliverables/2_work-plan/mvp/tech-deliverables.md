# Development Deliverables

## Index

1. [Abstract](#abstract)
2. [Libraries](#libraries)
    + 2.1 [CoBox Crypto](#cobox-crypto)
    + 2.2 [CoBox Identity](#cobox-identity)
    + 2.3 [CoBox Client](#cobox-client)
    + 2.4 [CoBox Server](#cobox-server)
    + 2.5 [CoBox Client Interface](#cobox-client-interface)
    + 2.6 [CoBox Message Schemas](#cobox-message-schemas)

## Abstract

The following section details an estimation of development deliverables contained under the libraries that will be pursued to develop an MVP. This is to provide a feature set that includes:

* An identity management dashboard
* Private group encryption
* Blind replication for remote distributed backup
* A shared file system
* An intuitive and accessible interface

## Libraries

The following are all NodeJS modules, beginning with low-level primitives and finishing with front-end interface.

### cobox-crypto

Cryptography primitives used by Cobox.

#### Considerations:
* All cryptography should aim to be compatible with the existing Dat ecosystem for future extensibility and interoperability
* All keys and message encryption / signing, when possible, should be executed inside a virtual environment for secure computation, such as Zenroom
* Zenroom currently doesn't support Dat-compatible cryptography

#### Deliverables:
* [ ] a symmetric key generator for creating shared secrets
* [ ] an `ed25519` asymmetric keypair generator for creating identities
* [ ] a hyperdrive compatible, crypto encoder encrypting file system changes and application layer data for groups
* [ ] generate a `READ_KEY` for sharing, enables read and write permissions in a cobox group: `concat(pubKey, sharedSecret)`
* [ ] generate an `ENCRYPTION_KEY` for write permission in a cobox group: `hash(pubKey, sharedSecret)`

### cobox-identity

This module will handle key management operations.

**Considerations**
* Keypairs need to be stored securely.
* Key backup mechanism will be required in future for easy usability and recovery of old encrypted messages

**Deliverables**

* [ ] use cobox-crypto to generate store asymmetric keypairs and store them securely and accessibly on the client device.
    * `$HOME/.cobox/config.yml` records identity public keys and associated name, and references the path of the secret key file, stored in `$HOME/.cobox/.secret_keys/` with `-r--------` permissions (`chmod 400`)
    * e.g. `~/.cobox/.secret_keys/8eef8a6cf768d4ebae3639aa75ca12fabc70e8321e95d7c8e8f36b487c4ab969` is a file that reveals a secret `51943865a4da9dc74fbf04cae24e3dbcabd47cc65573a145ebb4d60f7f87f8d8`
    * Possible example config file:
```
# config,yml
identities:
  - public: 8eef8a6cf768d4ebae3639aa75ca12fabc70e8321e95d7c8e8f36b487c4ab969
    secret: $HOME/.cobox/.secret_keys/8eef8a6cf768d4ebae3639aa75ca12fabc70e8321e95d7c8e8f36b487c4ab969
    name: 'Alice'
    groups:
      - 86b1c5952ed14aa5a024170dcfdedea3
  - public: da0fb7759f41ebb1bb83e213f33832520ecd5bbb9648b5ea81c0d9d8c3e35177
    secret: $HOME/.cobox/.secret_keys/da0fb7759f41ebb1bb83e213f33832520ecd5bbb9648b5ea81c0d9d8c3e35177
    name: 'Bob'
    groups:
      - 86b1c5952ed14aa5a024170dcfdedea3
groups:
  - public: 86b1c5952ed14aa5a024170dcfdedea3
    secret: $HOME/.cobox/.secret_keys/86b1c5952ed14aa5a024170dcfdedea3
```

* [ ] A threshold sharing scheme for backing up keypairs within an existing group
    * Port in Dark Crystal message types, workflow and crypto into hypercore. Secret sharing operations will happen in a separate module, `cobox-dark-crystal`, but `cobox-identity` will handle secure storage of ephemeral keys.

### cobox-client

This module handle cobox groups and their associated  filesystem and application data. It will cover creating a new group, connecting to an existing group, managing which groups are replicated by the server, and resolving conflicts in the filesystem.

#### Considerations
 - Our chosen model based on needfinding research.
 - Mechanism by which peers enter or leave a group. This is central to our prinicple of collective management of a Cobox.
 - Filesystem conflict resolution as described below.

**Pure p2p model**
* [ ] Create a group on a client device and store the `READ_KEY`. A group consists of a shared hyperdrive for each peer, and a hypercore for each peer.
  - [ ] `~/.cobox/config.yml` records group keys and stores the group key against the identity with which it is associated
  - [ ] Connects to `cobox-server` and sends the public key to cobox for replication
* [ ] Read from aggregated shared hyperdrives, giving up-to-date state of the filesystem.
    * **Basic:** Resolve the latest change using a file's `mtime`
    * **Intermediate:** Dynamically build a set of indexes that record hyperdrive file system changes. Each time a change to the file system is made, the index records a list referencing each previous to the file in question.
    * **Advanced:** Allow for diverging pathways of file system changes, and resolve using a conflict resolution / merging system. Flag any automatically unresolvable changes to the peer for them to resolve using the GUI.
* [ ] Extend a group to contain a 'personal' hyperdrive for each peer.
    * Other peers replicate this drive but do not aggregate this into their shared file system. This can be used for, for example, avatars.
    * It can also be ignored if peer's don't regard this as important
* [ ] Replicate a group of hyperdrives and hypercores using a single address / discovery key.
    * generate a top-level address / public key / discovery key by initialising a `_fake` hypercore or generating a read-only key
    * share a manifest between two connected peers detailing the public keys / ids of logs / drives they hold under a given 'top-level' address
    * replicate both ways, using each received discovery key

**Hybrid model**

This will involve scripts and software outside of this Node module.

- [ ] Connect to `cobox-server` API and initialise a group and group archive.
- [ ] The archive is a single encrypted hyperdrive, and the secret signing key is stored only on the server (as well as a distributed backup held by peers).
- [ ] Whitelisted peers are able to connect using traditional network filesystems (such as NFS, samba, sshfs).
- [ ] Changes are blind-replicated by partnering organisations.

**Common features to both models:**
  - [ ] Secure method of sending keys to other peers
  - [ ] Consensus mechanism of decided who enters or leaves the group
  - [ ] Consensus mechanism of deciding on partner organisations for blind replication
  - [ ] Component to blind replicate another group's cobox. In the case of the hybrid model this is already implemented in Dat but we will need an API so that this can be managed by the user interface.

### cobox-server

This module will provide operations only needed by the server.

**Considerations**
* Runs on the CoBox device itself.
* Once CoBox is plugged in, automatically begins serving an API accessible over the LAN
* Stores the owning organisation, and partner organisations data
* The server is 'dumb', it doesn't store any signing or writing keys, only public (blind) replication keys

**Deliverables**
* [ ] Serve a secure connection via the local network, either a web API or using UDP packets
* [ ] Accepts public keys over the local connection
* [ ] Validates uniqueness of received public keys against existing stored public keys
* [ ] Replicates archives of the stored public keys

### cobox-client-interface
This module will provide a GUI which will be run on the client device for identity management, accessing groups, personal settings, and viewing the file system.

#### Considerations:
- Accessibility
- Internationalisation
- Cross-platform support. Current proposal: Electron desktop app.

#### Deliverables:

* [ ] A simple application that connects to the client's cobox over the local network
* [ ] Send a request to the CoBox device to begin replicating a group using the blind replication key.
    * If CoBox is dumb, CoBox should only ever accept a `PUB_KEY`, rather than a `READ_KEY`, since CoBox never needs to read or write.
* [ ] Access cobox-crypto via cobox-core to generate a new asymmetric keypair (identity) and render in the interface
    * Publish an alias / name to a writable hypercore using the `about` message schema
    * Publish an avatar to a writable hypercore using the `about` message schema. Store the image itself in the writeable hyperdrive
* [ ] Access cobox-core to generate a new group
    * returns a `READ_KEY` and a `PUB_KEY` with which you can share with your peers. `READ_KEY` gives access to the group, `PUB_KEY` gives blind replication capacity
* [ ] Access cobox-core to read from a given group's shared file system
    * [ ] Bare bones interface like dropbox web / google drive web (render images, markdown, etc)
    * [ ] 'Download' a file to a local location
    * [ ] 'Upload' a file via the interface to append new changes to a file
* [ ] Front end for consensus mechanism
* [ ] A minimal settings dashboard

### cobox-message-schemas
This module provides a set of schemas and validators for JSON messages to be stored in our hypercore for application-layer data. Our MVP will require few schemas, but can be expanded upon to build more complex applications on the stack following initial MVP delivery.

**Deliverables:**
- [ ] A JSON schema for type `about` messages allowing name / alias and avatar as parameters
- [ ] A validator for JSON `about` messages
