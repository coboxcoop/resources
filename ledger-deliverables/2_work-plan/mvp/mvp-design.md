# Architecture Schematics Overview - Cobox MVP

## Index

1. [Components and Considerations for CoBox Back-End](#components-and-considerations-for-cobox-back-end)
  + 1.1 [Overview](#back-end-overview)
    + 1.1.1 [Kappa Architecture](#kappa-architecture)
    + 1.1.2 [Distributed File System](#distributed-file-system)
    + 1.1.3 [Conflict Resolution](#conflict-resolution)
    + 1.1.4 [De-duplication](#de-duplication)
    + 1.1.5 [Sparse Replication](#sparse-replication)
    + 1.1.5 [Blind Replication](#blind-replication)
    + 1.1.6 [Key Management](#key-management)
    + 1.1.7 [Anonymity](#anonymity)
    + 1.1.8 [Permissions, Private Groups, Revoking Access](#permissions-private-groups-revoking-access)
    + 1.1.9 [NAT Traversal](#nat-traversal)
    + 1.1.10 [Peer Discovery](#peer-discovery)
    + 1.1.11 [Offline First](#offline-first)
    + 1.1.12 [Decision Making Tools](#decision-making-tools)
    + 1.1.13 [Hardware](#hardware)
  + 1.2 [Three Proposed Models](#three-proposed-models)
2. [Components and Considerations for CoBox Front-End](#components-and-considerations-for-cobox-front-end)
 + 2.1 [Overview](#back-end-overview)
   + 2.1.1 [JavaScript Frameworks](#javascipt-frameworks)
   + 2.1.2 [On Boarding](#on-boarding)
   + 2.1.2 [UI Accessibility](#ui-accessibility)
 + 2.2 [Proposed Approaches](#proposed-approaches)
   + 2.2.1 [File System Mount](#file-system-mount)
   + 2.2.2 [System Tray Server, Websockets and the Browser](#system-tray-server-websockets-and-the-browser)
   + 2.2.3 [All-in-the-Browser](#all-in-the-browser)
   + 2.2.4 [Browser Extension](#browser-extension)
   + 2.2.5 [Electron Application](#electron-application)
   + 2.2.6 [Beaker Browser Integration](#beaker-browser-integration)

# Components and Considerations for Cobox Back-End

**Note:** Many of the modules listed here are experimental / work-in-progress

## Back-End Overview

### Kappa architecture

With traditional databases, everyone reads and writes from a dataset which is the single source of truth. With kappa architecture, the source of truth is represented by a materialised view generated from a set of append-only logs. Anybody can write data to their own log, but the state of the database is determined by conventions in how these logs are interpreted. 

- [kappa-core](https://github.com/kappa-db/kappa-core) is a system for creating materialised views over a set of append only-logs. [Cabal](https://cabal.chat/) is a IRC-like chat platform build on kappa-core.
- [kappa-view-query](https://github.com/KGibb8/kappa-view-query) allows map-filter-reduce queries on a kappa-core.
- [kappa-box](https://github.com/coboxcoop/kappa-box) demonstrates using [private-box](https://github.com/auditdrivencrypto/private-box) to encrypt messages between kappa-core feeds

### Distributed File System

Ideally we want a file store for the group which is resilient to the loss of any one device.

- [peerfs](https://github.com/karissa/peerfs) - A multiwriter peer-to-peer filesystem, built on kappa-core and hyperdrive
- [hyperdrive 10](https://github.com/mafintosh/hyperdrive/tree/v10) is not yet production ready, but addresses the multi-writer problem (as well as other problems) by using [hypertrie](https://github.com/mafintosh/hypertrie)
- [DatFS](https://github.com/fsteff/DatFS) - a proposal for a dropbox-like system based on Dat

### Conflict Resolution

This is a major issue in multi-writer distributed systems.  How to resolve conflicts when two peers make changes which 'clobber' each other (eg. making conflicting changes to the same file)?

The extent to which it makes sense to refine and optimise conflict resolution depends on the use case. Do we need to consider several people simultaniously writing to the same document? Or are we rather dealing with archived copies of documents created with some other collaboration tool?

- [automerge](https://github.com/automerge/automerge) is a javascript library for collaborative applications which uses a Conflict-Free Replicated Data Type (CRDT).  This means it provides a convention for interpreting updates to the data concurrently, without coordination between peers, and that there is a standard method of resolving inconsistencies.  Automerge's CRDT is based on the paper ['A Conflict-Free Replicated JSON Datatype'](https://arxiv.org/abs/1608.03960) by Kleppmann and Beresford

- [hypermerge](https://github.com/automerge/hypermerge) - combines automerge with Dat's Hypercore
- The medium article [Pixelpusher: Real-time peer-to-peer collaboration with React](https://medium.com/@pvh/pixelpusher-real-time-peer-to-peer-collaboration-with-react-7c7bc8ecbf74) is a great explanation of the motivations and practicalities of building collaborative apps without servers
- [Trellis](https://github.com/automerge/trellis) a distributed, offline-first clone of Trello (kanban task management), built with Automerge

### De-Duplication

This is another big problem in distributed systems - how to optimise disk usage by avoiding that the same data is stored more than once. The extent to which we want to prioritise this relates to the use-cases we are catering for - do we want to account for large amounts of media files.

DAT's hyperdrive stores data in chunks, but changes to a file are represented by a complete copy of the updated file, which means a large file with many small amendments uses a very large amount of space. 

Furthermore, there is no strategy for de-duplication between hypercores, meaning that if there are several hyperdrives which contain many files in common, these files will be stored several times. 

### Sparse Replication

Linked to the storage concerns outlined above, sparse replication would allow peers to only replicate the parts of data they are interested in. 

### Blind Replication

Blind replication refers to being able to replicate a data set without being able to read it's contents. This would allow a group to create a dynamic backup of another group's data without being able to read it themselves, thus providing backup storage without needing to take responsibility for holding the content. This should be combined with a key management technique so that the encrypted data can be retrieved in the case of data loss.

This is an important feature for Cobox as it will allow affiliated co-operatives to backup each other's data, following the principle of mutual aid.

- [ciphercore](https://github.com/telamon/ciphercore) a wrapper for hypercore that provides content encryption, allowing blind replication
- [hypercore-encrypted](https://www.npmjs.com/package/hypercore-encrypted) a wrapper around hypercore that supports encryption

Blind replication can be done by encrypting all content in a hyperdrive / hypercore instance using a shared secret / a symmetric encryption key. Read access to the repository is achieved through replicating across a p2p network using the public key, and decrypting the hypercore logs locally using the encryption key. This can be shared with others over a secure connection by concatenating the public key with the shared secret.

### Key Management

Issues with key management are another major problem in distributed systems. What happens when someone looses a device? Can access be revoked if a key is compromised? Is it possible to restore an account or retrieve old messages when a key is lost?

- [dark crystal](https://darkcrystal.pw/) a system for social backups using threshold-based secret sharing
- [research for dark crystal](https://github.com/blockades/mmt_resources/tree/master/research)

If we take a pure p2p approach, we can build an identity dashboard, which will allow peers locally generate cryptographic identitys, and use these different identities / aliases in different group contexts. This will not stop people from using the same identity in multiple contexts, allowing for a peer to manage their own representation within a given world. Such a identity dashboard will require a smart social key management system such as specified above. 

### Anonymity

Is it important that IP addresses are invisible to other peers? 

- [Tor DAM](https://github.com/parazyd/tor-dam) is a 'distributed announcement mechanism' allowing anonymous peer discovery over the Tor network. Uses Tor 'hidden services' (also known as 'rendez-vous points').

### Permissions, Private Groups, Revoking Access

Private groups are a difficult problem in cryptography. Making existing material available to a new member, or revoking access from a member who has left the group.

As a horizontal governance tool we want a way of managing group members which does not give an imbalanced power dynamic.

### NAT Traversal

Another major difficulty with peer-to-peer systems is NAT traversal. How can you communicate directly with a peer who is behind a Network Address Translator? Port forwarding is one option, but ideally peers should not need to reconfigure their router (as this can be difficult for non-technical people and not everybody has access to their router). Universal plug and play (uPnP) is another option but it has security issues. 

### Peer discovery

Supposing the NAT traversal problem is solved and you have a way by which they can be addressed, you still need a way of finding their addresses.

Techniques for going about this include distributed hash tables (DHTs), and gossip protocols.

- [discovery-swarm](https://github.com/mafintosh/discovery-swarm) The peer discovery system used by DAT. Uses the Bittorrent DHT, centralized DNS servers and Multicast DNS simultaneously.  In practice, the centralised services work so well that the DHT is rarely used.
- [hyperswarm](https://github.com/hyperswarm) - 'a distributed networking stack for discovering peers' consisting of a DHT which uses 'hole punching' for NAT traversal.
- [Tor DAM](https://github.com/parazyd/tor-dam) (mentioned above) is also a peer discovery mechanism.

### Offline-first

Many of the peer-to-peer technologies we have been looking at have the property of continuing to function offline or with poor/intermittent internet connectivity.  How important is this feature? 

### Decision making tools

An important tool for horizontal governance is a way of recording opinions to come to a decision.

- [loomio](https://www.loomio.org/)
- [ssb-poll](https://github.com/ssbc/ssb-poll-schema)

### Hardware

What role does the physical device play?  Does it even need to be part of the MVP?  Is it possible to use cobox without one?  To what extent will Cobox be taylored to particular hardware?

- [A20-OLinuXino-LIME2](https://www.olimex.com/Products/OLinuXino/A20/A20-OLinuXino-LIME2/open-source-hardware) Open source hardware, ARM Linux single board computer 

## Three proposed models

Cobox aims to address the limitations of using currently commercially available cloud services, or currently available self-hosted solutions.

Regarding self-hosted solutions, a major concern for cooperatives, who typically want a horizontal governance structure, is that control of the server essentially remains in the hands of those who administer it and those who have physical access to it.  The latter is of particular concern for distributed teams who will have to choose a physical location for their self-hosted device.

We have come up with two models regarding the role of the physical device.

### Proposal 1: 'SuperNode'

![proposal 1](./cobox1.svg)

Peers access web-apps hosted on CoBox via a traditional client/server connection. For example, CoBox runs an instance of NextCloud. Peers access the interface via localhost or a web domain (we’d need DDNS setup - owch). CoBox then periodically dumps the NextCloud database (which stores all file data and files) and appends them to a ciphercore instance, so partner organisations that hold the dat public key can blind replicate the data dump. We’d need to work on a diff system so minimise duplication. Should the cobox device break, be stolen, or taken offline, it can be restored from backup to a new device and keys restored using a social recovery technique.

#### Description
- Cobox uses traditional client/server model for serving applications and file system 
- Runs periodic backup of apps to a peer-to-peer encrypted feed for blind replication by other co-ops 
- Other co-ops replicate
- Peers don't host data on their devices

#### Pros
- Easier to use with existing collaboration tools and platforms
- User familiarity - everything goes through the browser, using applications they are familiar with (nextcloud / loomio / cobudget)
- Little to no front-end work if we choose - we could just run a nextcloud instance on CoBox
- Initially more aligned with Heimdall's roadmap for application market place

#### Cons
- Not easily scalable. Size of ciphercore / cipherdrive feeds would quickly become bloated if we're appending a complete database dump. Research to be done around publishing diffs.
- Availability dependent on uptime of device

### Proposal 2: 'Peer to Peer'

![proposal 2](./cobox2.svg)

The pure peer-to-peer model treats CoBox as a stable omniscient peer in a peer-to-peer network - CoBox is not acting as a traditional server. Each peer generates a hyperdrive instance on their device and mounts this to a location on their computer. Each peer's hyperdrive is pinned to a shared read key (in the case of read/write access) or public key (in the case of blind replication). This key effectively creates a 'group', and using this key, peers can replicate each other peer's latest representation of the group / repository / file system and aggregate them. When making a change, peers write changes to their own hyperdrive instance, and broadcast these changes to the network. State changes to the file system can be aggregated simply using `mtime` (which will come with a set of difficulties), or by appending to a hypercore instance that records state changes by linking back to previous changes to the file. The hyperdrive in which the latest version of a given file residers can be ascertained using the aggretated state feeds, which will record the feed public key and the hypercore sequence number of the latest change in content.

There are challenges to this approach, some of which are currently on the roadmap for Hyperdrive V10, to be completed by the end of 2019.

1. Aggregated hyperdrives / effectively a multi-write peer-to-peer file system, are to be completed by the core Dat development team by December 2019, and this is being resolved using a CRDT (Conflict Resistant Data Type), enabling smooth file conflict resolution.
2. Due to the de-duplication issue that hyperdrive currently has, not only are there are many versions of a file system stored on a peer's device at a given time, but each hyperdrive after each change appends the entire contents of the file to the content log, effectively meaning any state changes generates a whole extra copy of the file. De-duplication in hyperdrive means that storage, particularly when handling large files, very rapidly becomes unnecessarily bloated.

Storage concerns on a client-device can be addressed to some extend using sparse replication - a peer only replicates the content that they ask for, rather than the entire repository - meaning client devices would end up using significantly less storage space than CoBox / other blind replicators (which would still need to host the entire dataset). Sparse replication is part of the development roadmap for Hyperdrive V10.

The beauty of this approach is that CoBox is never a security libility - it does not have write capacity, and does not hold any keys - it is a blind replicator. Only members hold the write and encryption keys. So the CoBox network is comprised of other blind replicators that ensure system uptime. Should the organisations personal CoBox be taken offline, peers can continue to replicate from each-other, and/or from the partner organisations that blind replicate that specific repository. In this case then, the more partner organisations you have, the more stable your own file system.

This approach will be more work to implement in the short term, but comes with the benefits and challenges of a genuinely peer-to-peer system.

#### Description
- pure p2p
- Cobox is omniscient stable peer, replicates everything
- Peers can replicate sparsely to save space on client device
- Peers append to their own local feeds before replication
- All peers (including CoBox and backup partner co-ops) replicate

#### Pros
- Peers host data locally, so if Cobox goes down, data is accessible
- Peers can talk directly to eachother, so can also get updates from peers who are online
- All partner organisation act as nodes to enable blind replication - Cobox isn't the only peer to facilitate this
- Cobox device is a hub for data availability, ensuring system uptime
- Sparse replication can lighten the load on client devices
- Cobox device is dumb - it doesn't hold any keys - so loss / theft of CoBox isn't a security issue 
- Loss of keys by a peer isn't super important, as long as they can get the read access key from a fellow co-op member. Peers can generate a new identity and claim the old identity as theirs.

#### Cons
- More development work than the other two approaches, more moving parts in the p2p stack
- Multiple keys to manage, in the long run will need an intelligent and smooth key management system (Dark Crystal) 
- We can't easily integrate traditional client/server applications. To use existing Loomio app, we'd need to build middleware to change Loomio's backend.
- De-duplication means file system can very quickly become bloated, especially if handling large files.
- Conflict resolution of files in a peer-to-peer system needs to be managed using a CRDT or other conflict resolution mechanism

### Proposal 3: 'Hybrid'

![proposal 3](./cobox3.svg)

Peers mount a network-attached-storage (NAS) filesystem using a conventional client/server method such as NFS, Samba/cifs or sshfs. On the CoBox itself, which serves the file system, the mounted drive is setup as an encrypted hyperdrive instance. Changes to the file system over the NAS are appended to a transaction log, so CoBox can regulate possible conflicts when multiple users are changing files at the same time. CoBox holds the symmetric encryption key and uses this to encrypts changes to the file system and write these to the hyperdrive instance. Partner organisations who hold the public key can blind relipicate this hyperdrive instance. Members of the organisation could (if they so choose) who hold the read key may replicate and read the data in the hyperdrive instance. The read key is defined as `concat(pubKey, sharedSecret)`. Its important to note, with the hybrid model, only CoBox holds the signing key for the hyperdrive instance, so only CoBox can make changes to the hyperdrive file system. If members want to make changes to the file system, they have to use the NAS, but they can access the files offline as read only after replication.

#### Description 
- Files shared over net / LAN directly to cobox
- Cobox stores in encrypted feed
- Peers replicate sparsely from cobox

#### Pros
- Less keys to manage than pure p2p
  - Only keys needing backup are those on the cobox
  - Peers don't have to worry about backing up their own feed keys
- Good for poor connectivity, data is available as you store data on your device
- Simplified / transactional conflict resolution - Cobox (not peers) gets to decide how to resolve conflicts
- All peers are nodes enabling blind replication

#### Cons
- Uptime is threatened by loss / failure of cobox device.
- You can no longer receive updates other people make, but you still have the last known state of the files on your device.
- Possible high storage requirements for peers

# Components and considerations for Cobox front-end

## Front-End Overview

### JavaScript Frameworks

### On Boarding

A major challenge to facilitate replication, blind or otherwise, is how peers are securely given the required keys. In addition, co-ops who back-up each-others data will need to ascertain the blind replication key in order to replicate their data. A major challenge will be enabling new co-op members to join the shared file system with either read or write access while making this intuitive for users familiar with current web 2.0 patterns. There are several challenges to enable easy on-boarding of users without leaking either the public keys or the encryption keys.

### UI Accessibility

Accessibility and ease of use of a UI is a difficult challenge for peer-to-peer applications. Without hosting all the data directly in the browser, p2p systems reside in a challenging space for easy on-boarding of users. Technology users want as little installation and technical requirements as possible. The reason browsers and web applications have been so successful is precisely because of the ease of getting software up and running without additional technical overheads. Without a traditional centralised remote server to serve an application over the web, p2p systems need to address these user requirements and attempt to enable their software in the browser. Beaker Browser has done much to build an intuitive browser-like interface which can also access local Dats stored in your file system, and it has done so by building a browser as an electron application, cutting across the restrictions that browsers face in accessing a local file system. Its important to note, these restrictions are in place for good security reasons, for example, to prevent cross-site scripting that could access a local file system.

## Proposed Approaches

### File System Mount

For a shared file system, we can mount this as a path locally on your computer using software such as `hyperdrive-daemon`.

#### Pros
- Simple and intuitive interface, most modern users are familiar with navigating their computer's file system

#### Cons
- Limited capability. It would have to be bootstrapped with either an electron application or browser application for identity management, permissions, new group generation and other higher level operations that don't directly involve the file system.

### System Tray Server, Websockets and the Browser

The repository / group / file system is shared using a system tray application to serve data over websockets to the browser. Users can access the interface over localhost.

#### Pros
- User familiarity and accessibility

#### Cons
- Accessing `localhost` with a port number isn't exactly a familiar practice for most contemporary internet users. This could likely be partially resolved with a local DNS server system such as `dnsmasq`, allowing users to define their a local url which their browser can correctly interpret.
- A system tray application needs to be installed and run on machine start in order to serve data to the browser over web sockets 

### All-in-the-Browser

All hyperdrive / hypercore data is bundled using a tool such as `browserify` or `webpack` and stored in the browser. This removes the need for using websockets and user interface code can be bundled into a single `index.html` file. The CoBox interface can access all hyperdrive data directly in the browser.

**Unknowns:**
- Where do we store keys?
- Is this secure?

#### Pros
- If applications become bloated, the browser's cache can easily be cleared and its possibe files could be re-replicated using fresh sparse replication parameters.

#### Cons
- Since all data is stored in the browser, accidentally clearing the browser's cache would remove all data from the device.
- Accessing via an `index.html` still isn't the most intuitive way to load an app. This could also be bootstrapped by navigating to `http://app.cobox.cloud` which redirects you to a set path on your computer where the index.html file is stored. However, that requires internet connectivity.
- The browser itself would likely become bloated and memory intensive.

### Browser Extension

**Unknowns:**

#### Pros

#### Cons
- Different browsers have their own extension integrations. We would have to build an extension integration for each browser. This may be particularly painful / challenging when it comes to Microsoft's IE.
- Significant on-going maintenance and familiarity with multiple ecosystems (Firefox, Chrome, Internet Explorer).

### Electron Application

CoBox is bundled into a cross-platform electron application. 

#### Pros
- Developer familiarity
- Quick to spin up and proof-of-concept interface codebase, which can easily be migrated later into a browser 

#### Cons
- Users are required to install a client application
- Desktop / laptop only, mobile phones not supported

### Beaker Browser Integration

Work with the Beaker Browser team to enable `cobox://` urls in the browser.

#### Pros
- Removes the need for websockets or other more complex mechanisms to access local storage, as beaker is an electron application, so can already access local storage.

#### Cons
- Requires users to install a new application locally, however this is significantly eased by the fact that Beaker is a next generation browser.
- Beaker are not going to invest time and energy in an integration until the project is more established
