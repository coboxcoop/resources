# Implementation and Testing

## Free and Open Source

A good portion of development time will be used to develop the tools we plan on using from the wider ecosystem. In order to enable the features we need for CoBox, we will be submitting pull requests to other shared code-bases. 

Any brand new source code in early stages will be kept in a private team git repository. Packages will be published to github, git-ssb, npm, ssb-npm, and made freely available and open source as they reach maturity and limited battle-hardiness.

## Development Plan

### Overview
We are developing in NodeJS.

At the beginning of each week, developers will attend a stand-up meeting reviewing the previous week's development, ascertaining this sprint's milestones, and splitting up tasks for the week ahead. This will be done using the kanban.

Development will proceed in 2 week sprint cycles. Each sprint cycle will focus on hitting a particular milestone.

### Milestones

Our milestones will be mapped following the outcome of the need-finding report and peer-centered design sessions being conducted in August 

### Workflow

#### Project Management

* ledger kanban board - list deck for organising and managing development sprints, features, deliverables and bugs 
* ledger gitea - private git repository for ledger participants 
* git-ssb - distributed public git on secure scuttlebutt for more mature code 
* github - centrally hosted public git
* ssb-npm - distributed package manager
* npm - centrally hosted package manager

#### Testing Framework

All code submitted must be test covered and reviewed by at least one other team member. Code will not be merged unless adequate testing can be displayed.

Please use `tape` or `tape-plus` combined with `tap-spec`. An example test configuration:

```json
{
  "scripts": {
    "test": "tape test/**/*.test.js | tap-spec"
  },
  "devDependencies": {
    "tap-spec": "^5.0.0",
    "tape": "^4.11.0",
    "tape-plus": "^1.0.0",
  }
}
```

## Testing

By early November, we aim to have a minimal working CoBox device. At this stage, each CoBox team member will receive a prototype device, which they should place in a secure location. The team will then securely share the read access key to each team member and attempt to share files. By this stage, conflict resolution will be minimal / dumb and will be resolved later in the development phase, so team members should not expect shared files to necessarily write correctly.

### Local Hosting - Prototype Troubleshooting
In order to enable the team to test the minimum functionality of the hardware and software, the following feature set will be required:

* A CLI command for joining a cobox group (using the `readKey`)
* Read access to a shared file system
* A CLI command to listen for remote changes to the shared file system 
* Write capability for your own files
* Replication across a network
* A minimal file system interface

### Peer-centered Design Session 2
By the second peer-design session at the beginning of December, we need a stable-enough device to test with our peer-groups in order to achieve significant useful feedback. The team's local / self-hosting sessions during November should help to expose many of the initial bugs and challenges, with an additional sprint within which to iron out issues.

### Local Hosting 2 - Integrating Further Features
If possible, by the beginning of January, we would like to have:

* A easily accessible user interface for access management
* Write capability of each-other's files (smart conflict resolution) 
* Blind replication / backup using a cobox group's `publicKey`

### Peer-centered Design Sessions 3 & 4 
Final review of the MVP with partner organisations, with a full technical demonstration and presentation of current features. Co-conceiving and co-designing a roadmap for additional features and UI improvements to integrate into future development.

### Further / Future Features
* Sparse replication to client devices
* Back-up check-in / guarantee
* Social key management using a threshold scheme 
* Optional privacy extension using TorDAM

### Security Review

In January, we will:
* conduct an external independent review of the device's security to expose possible attack vectors and vulnerabilities
* conduct an external independent review of the cryptography used in the project
* conduct a review with our researcher-in-residence, reviewing privacy features and possible vulnerabilities. This will integrate feedback from our peer review and need-finding to determine the importance / emphasis placed on anonymity and, if required, roadmap it's integration in a further development phase.

