![cobox-icon](./assets/cobox-icon.png)

# LEDGER MVP Technology Readiness Level 4-5 ~ CoBox

## Index

1. [Abstract](#1-abstract)
2. [The MVP](#2-the-mvp)
  + 2.1 [Overview](#21-stack-overview)
  + 2.2 [Dat and Kappa](#22-dat-and-kappa)
  + 2.3 [Key Concepts](#23-key-concepts)
  + 2.4 [Key Features](#23-key-features)
    + 2.4.1 [Distributed: The Multi-Writer File System and Conflict Resolution](#241-distributed-the-multi-writer-file-system-and-conflict-resolution)
    + 2.4.2 [Privacy with Persistence: Content Encryption and Blind Replication](#242-privacy-with-persistence-content-encryption-and-blind-replication)
    + 2.4.3 [Accessibility: Too Many Keys, 'Account Backup' and Key Derivation](#243-accessibility-too-many-keys-account-backup-and-key-derivation)
    + 2.4.4 [Authentication and Authorization: Access and Permissions](#244-authentication-and-authorization-access-and-permissions)
    + 2.4.5 [Usability: Intuitive User Interfaces](#245-usability-intuitive-user-interfaces)
  + 2.5 [Packages](#25-packages)
  + 2.6 [Reflection](#26-reflection)
3. [The Future](#27-the-future)
  + 3.1 [Future Features](#31-future-features)
    + 3.1.1 [Health Check](#311-health-check)
    + 3.1.2 [Match-Making and the Registry](#312-match-making-and-the-registry)
    + 3.1.3 [Improved Authentication](#313-improved-authentication)
    + 3.1.4 [Nested Group Authorization](#314-nested-group-authorization)
  + 3.2 [Core Updates](#31-core-updates)
    + 3.2.1 [KappaCore update](#321-kappa-core-update)


## 1. Abstract

Our technical MVP address the data storage, accessibility, hosting and archiving needs of the co-operative sector by proposing a distributed file storage and replication mechanism.

Following a protracted period of intense need finding research at the inception and first few months of the project, our team whittled down the need finding report into a clear set of aims and deliverables for the MVP that would allow for the extensibility of the system while also aiming to meet the desires and needs of a broad a base of interviewed peers as possible. This was completed alongside developing the necessary lower level prototype components required to enable the kind of features we correctly envisioned coming out of the need-finding report.

Throughout this paper we will refer back to the aforementioned need finding report, in addition to the 'Mock Up' and 'MVP' documents produced in the prior months of the LEDGER programme. Points in our development process at which we have deviated from these documents can be explained by a change in design and approach following the integration of our need finding research in November 2019 into our development roadmap.

## 2. The MVP

### 2.1 Overview

In order to deliver the MVP, balancing our initial mockup while ensuring our need finding research was properly integrated into our development roadmap, our team prioritised two key features. These commonly noted and necessary features for early adoption were:

> * Implement blind replication as a priority for all mutual back-ups.
>
> * Fast and safe ways to back up user keys are critical.
>
> -- [cobox in context, pp. 2](https://ledger-git.dyne.org/CoBox/cobox-resources/src/branch/master/ledger-deliverables/3_mock-up/assets/cobox-in-context_need-finding-report-2019.pdf)

Several pre-requestites were required to enable blind replication of a shared file system - namely, the major challenge, a multi-writer p2p file system. Following to ensure blind replication and privacy, we prioritised content encryption and asymmetric cryptographic authentication. This enabled us to build private spaces for groups to organise, persist files and sync information across a local or remote connection.

### 2.2 Dat and Kappa

Dat is particularly well suited to scaleable file storage systems, and will continue to be developed in such a way by the core engineers working at the base of the stack. Importantly, Dat and the Kappa setup is offline-first. Data is persisted locally, and changes are sync'ed across any available network connection. Several key features of Dat and the Kappa stack fulfilled some of the need-finding's specifications.

> * Ensure that no single node can accidentally become responsible for destroying or
exposing sensitive data belonging to someone else.
>
> * Manage transfers of larger back-ups without crippling network speeds.
>
> * Work should be possible online or offline.
>
> * Ease of retrieval in case of local loss.
>
> -- [cobox in context, pp. 2-3](https://ledger-git.dyne.org/CoBox/cobox-resources/src/branch/master/ledger-deliverables/3_mock-up/assets/cobox-in-context_need-finding-report-2019.pdf)

In spite of this, while delivering the MVP we butted up against a number of existing challenges that remain only partially or un-resolved in the Dat ecosystem. These were clearly documented in our mock up document. [REF] Our team saw this as an opportunity to contribute to a growing network of p2p innovators using these technologies, to expand the base of tools available to other developers in this OSS community. Drawing inspiration from existing project Cabal, a first implementation of a multi-writer application using the kappa-db stack (built on Dat), CoBox took a similar approach, using kappa-db's tools as a basis to innovate further. Below we have documented some of the key challenges we faced, along with the solutions proposed and implemented in the MVP required to enable a minimal peer-to-peer cloud storage solution.

### 2.3 Key Concepts

#### 2.3.1 Private Group

It became clear early that in order to provide a sovereign and secure, private cloud infrastructure rooted in commons economics and using a distributed system such as Dat, that we would need to assemble private cryptographic groups. In order for these groups to be viable for every day use, they must be easily discoverable by new peers, accessible for verified / permitted peers to authenticate and gain access, while solid and secure for those already internal to the group. Our early recognition was clearly articulated by interviewees and ratified in our need-finding report.

#### 2.3.2 Address

An address is a commonly recognised identifier that an individual group shares. Using this address, peers may meet other group members or blind replicators (see below) on the distributed hash table, create a secure encrypted connection, and sync the latest changes across each device.

#### 2.3.3 Blind Replicator

A blind replicator is a peer that remains connected to a given address to guarantee replication and backup of the latest changes made by group members. Blind replicators can be setup to run from your home, a partner organisations office, or other location with a stable network connection, to ensure that your group's backup retains integrity. Our interface provides an intuitive mechanism by which CoBox device owners can configure their device as blind replicators, form partnerships with different peer organisations to back up their groups. These actions are performed by any number of authenticated device administrators. Replication occurs by meeting that group's peers at the address, and persisting the latest encrypted changes locally.

### 2.4 Key Features

#### 2.4.1 Distributed: The Multi-Writer File System and Conflict Resolution

##### Multi-Writer File System

In order to build a distributed cloud system, behaving like Dropbox or Google Drive, we needed a multi-writer file system. Dat archives, known as hyperdrives, currently enable only single-write actions. Each hyperdrive is composed of two hypercore append-only logs, each hypercore log has a public and a signing key. Only those who hold the secret signing key can make any changes to a hypercore. Therefore, in order to produce multi-writer Dat archives, whereby _n_ peers could participate in a view on a file system, we needed to work from the base that:

1. Each peer can has write access to their own hyperdrive (they hold the signing key).
2. Each peer has a read-only copy of other peer's hyperdrives locally.
3. A multi-writer archive is the aggregated state of all hyperdrives under a shared address or public key.

To achieve these key requiremnts, we made use of kappa-db's multifeed, a hypercore aggregation and replication manager, along with kappa-core, an index plugin for multifeed. Using these tools our team built a tool called kappa-drive, which creates an abstration of a file system across a set of hypercores using a set of local indexes stored in leveldb.

##### Conflict Resolution

Once we had a global representation on a given file system, we began to take a look at resolving conflicts. Due to the distributed / local-first nature of the protocol, conflict resolution is necessary. Given that both Alice and Bob have made a declaration about a file when it was at different states, once syncing, Alice and Bob need to arrive at the same (or as close as possible) conclusion about the current state of a given file and the file system overall. In order to do so, we included a rudimentary implementation of vector clocks, a popular conflict resolusion mechanism for distributed systems.

![Vector clock diagram](https://ledger-git.dyne.org/CoBox/cobox-resources/raw/commit/951c7ea70bb09d7a03d355fa2171d1dba1dff651/ledger-deliverables/4_MVP/assets/vector-clock.png)

> _With a vector clock, time is measured logically in terms of 'number of events experienced'. Each peer maintains a 'vector' composed of their subjective clock for each other peer. When a peer experiences an event (a file system change in our case) they increment their own clock, and publish their version of the vector. These vectors can be merged by 'adding' them, and compared, to give a notion of time baed on causality_

Vector Clocks autonomously detect causality violations by comparing logical timestamps. This significantly improved the performance of the drive. There is more room for improvement. Future development in this area would concern itself with introducing the 'bloom clock', using false positives to infer a partial ordering of events with high confidence.

#### 2.4.2 Privacy with Persistence: Content Encryption and Blind Replication

##### Content Encryption
In order to enable a peer-to-peer, co-operative cloud, that ensures privacy-by-design, we had to guarantee that any partnership organisations replicating and backing up a group's data on their own CoBox device, must not be able to access the data contained therein. Similarly, were any leak of a groups public address to occur, we must be able to guarantee that even if an attacker were to replicate the data, having it would be effectively useless.

> Encryption on top of CoBox should be seamless.
>
> [cobox in context, pp. 2](https://ledger-git.dyne.org/CoBox/cobox-resources/src/branch/master/ledger-deliverables/3_mock-up/assets/cobox-in-context_need-finding-report-2019.pdf)

To resolve this challenge, we determined that in order to gain full access and knowledge to a group, access its contents, we must have some kind of shared key or passphrase between group members. Each group currently relies on a single 32 byte symmetric encryption key used both to encrypt/authenticate and verify/decrypt messages. This is rendered in our key export system as a BIP39 mnemonic and easily human readable for communication out-of-band. Hypercore, the append-only log that lies at the base of the stack that replicate across the hash table, permits the use of a custom encoder when appending content to the log. Using the secretbox API in javascript library sodium-native - which provides bindings to C library libsodium - our team implemented our own cryptographic encoder for each hypercore's content. In order to keep encryption keys secure, as any possible leak of a key would allow an attacker to decrypt the group's content, they have been made unavailable through our interfaces. Instead, as detailed in the [authentication](#244-authentication-and-authorisation-access-and-permissions) section below, we have included an invite code system.

##### Blind Replication

![Blind replication](https://ledger-git.dyne.org/CoBox/cobox-resources/raw/branch/master/ledger-deliverables/3_mock-up/assets/blind.png)

With content encryption achieved, blind replicators can easily be established with partner organisations, by simply sharing the group's address with the partner organisations device administrator. The CoBox hardware device - setup with cobox-os which comes with pre-installed package cobox-hub - is setup to receive commands from  authorised administrators established on device setup. Once verified, device admins communicate over the DHT with the device across the hypercore-protocol using a device specific unique address & encryption key combination - much like regular private groups. Across this connection, device administrators can send commands to a CoBox device to begin or stop replicating a given address. This is made accessible through the adminsitration panel in the interface.

Our future development roadmap will begin to implement some of the core desired features specified by our early adopters in the need finding report, such as customisable replication scheduling, ensuring administrators can manage network latency and switch their cobox device on and off at given times. There is currently no cap for on the amount of data a device will blind replicate for specific groups. Future development will allow administrators to configure their device's space capacity. For those peers requiring more support, device administrators can specific a top-level limit to ensure replication does not cease when backing up additional organisations data. These two core administration features create space for dynamic partnership agreements based on storage capacity and network uptime. In addition, new features added within the Dat stack enable us to be more granular about our replication policies, allowing group client devices to be more specific about what files and content they need, enabling peers to manage local storage space more efficiently.

#### 2.4.3 Accessibility: Too Many Keys, 'Account Backup' and Key Derivation

In our [mock-up](#LINK_ME_PLEASE) document, we detailed one of the central adoption challenges cryptographic distributed systems face: _key management_. What happens when someone looses a device? Can access be revoked if a key is compromised? Is it possible to restore an account or retrieve old messages when a key is lost?

Systems where peers manage their own keys are empowering, since peers themselves have ultimate control over their data. But this comes at a cost. There is no trusted intermediary who can intervene when things go wrong, such as by resetting the account.

Our MVP addresses these issues in two ways. Firstly, by minimising the dangers associated with keys being lost or compromised, and secondly by offerring and encouraging good key management techniques.

##### Minimising the risks associated with poor key management

Controlling group membership is a hard problem in cryptography, because once somebody has a key there is no way it can be taken away from them - access can be granted but not revoked. A common solution to this is to revoke access by creating a new group, distribute the new key to the desire group members, and import data from the old group. The cost of this approach is that the service is interrupted, and that a new address needs to be established in order to avoid duplicating the existing encrypted data, which in our case would mean blind replicators need to be reconfigured for the new group.

As detailed in the following section, ["Authentication and Authorisation: Access and Permissions"](#244-authentication-and-authorisation-access-and-permissions), peers are authenticated before replication occurs. This means the group does not need to be forked following a lost key. If, for example, a peer loses a device holding keys, they can simply create a new account on a device,  recieve the encryption key via an invite code, authenticate with the relevant group and continue to use the same group as before without it needing to be re-initialised. Future features can be implemented to allow peers to easily 'tombstone' or declare multiple different device public keys to be the same person, easing possible confusion arising out of old expired public keys being present in the group.

Reducing the effect of lost of stolen keys direcly addresses a key requirement in the need finding report:

> _"Fast and safe ways to back up user keys are critical. Given the challenges many people face in choosing and managing secure passwords this is the area that could make or break CoBox."_

##### Key derivation - One Key to Rule them All

![Key derivation](https://i.imgur.com/WKR6RnC.png)
Cobox uses many different keys, which would make backing them up and recovering them very complicated. To simplify this problem we have used a key derivation function. All keys specific to a single peer / device are derived from a single parent key. Signing keys for each group are kept unique by including the group address in the derivation function. The implication is that each peer / device can be secured and recovered using only one 32 byte parent key.

In addition to their own parent key, group members should also safeguard their group encryption keys. These are not derived from a particular peer's key because they belong to the whole group, so all members should take care of them.

##### Storage of secret keys

The parent key, and encryption keys for each group, are stored on the local filesystem, and are never exposed in the HTTP API. They are stored as binary files with restrictive permissions allowing only read access by the individual peer.

##### Paper backups

Our current key backup mechanism is simple but robust. A printable PDF can be generated and keys printed on paper, which should then be stored in a secure location. The PDF can then be deleted using a secure deletion mechanism. We feel this is a solution which also appeals to non-technical peers.

![Paper keys example](https://ledger-git.dyne.org/CoBox/cobox-resources/raw/commit/3d9d2a3e11ff09b5172f234b13f3dade8176adc9/ledger-deliverables/4_MVP/assets/paper-key-example.png)

Mnemonics are used to discorage mistakes when manually enterring a key read from paper.

#### 2.4.4 Authentication and Authorisation: Access and Permissions

To implement a stable authentication mechanism, we determined to make use of asymmetric cryptographic keys for each peer's device. As detailed in the previous sections, we are able to deterministically derive a single keypair for each device from a base 'parent key' - see ["Key derivation - One Key to Rule them All"](#key-derivation-one-key-to-rule-them-all).

We have designed an invitation protocol for on-boarding new peers to a private group simply over either a LAN, or from a distance using a clickable invite code served out of band, to ease the process while ensuring authentication. We also plan on developing this further, removing the need for the invite code altogether.

Currently, groups internally generate an 'allow-list', a summary of known public keys of all group members. Alice has sent Bob a copy of her public key, and Bob has shared that in the group. This means that when Alice attempts to gain access to a group, she can authenticate with whoever it is in the group she is connecting to. When swarming on the DHT, after the initial noise handshake and a secure connection is established, Alice meets Clare, and exchanges signatures. Clare knows Alice's public key because she is in the same group as Bob and can draw from her group's copy of the allow-list, so can authenticate Alice, and Alice is given permission to replicate from Clare. Derek attempts to join at the same time as Alice, but Derek's public key has not been placed on the groups allow-list, and so because he cannot provide a valid signature, he will fail to authenticate with Clare.

While it may be the case that this helps lock out unknown peers, it does not necessarily smooth the process of sharing the content encryption key. In the above scenario, known blind replicators are authenticated just like regular group members are. In order to smooth the process for Alice to gain access to the group, content and all, though she does not hold the encryption key, we have derived two approaches, one of which we have implemented in the MVP, another of which we envision for future development. In our current implementation, Bob sends Alice back an invite code, which is a copy of the group address and its encryption key, encrypted with her public key. This is then plugged into the UI, and Alice has full and secure access to the group.

#### 2.4.5 Usability: Intuitive Interfaces talking to a REST API and mounting locally with FUSE

##### Intro

In the [Architecture Schematics Overview - Cobox MVP](https://ledger-git.dyne.org/CoBox/cobox-resources/src/branch/master/ledger-deliverables/2_work-plan/mvp/mvp-design.md#components-and-considerations-for-cobox-front-end) (from [CoBox Ledger Workplan](https://ledger-git.dyne.org/CoBox/cobox-resources/src/branch/master/ledger-deliverables/2_work-plan/mvp/tech-deliverables.md#cobox-client-interface)) the challenges of onboarding peers into P2P systems and the subsequent UI accessibility issues these entail were highlighted. The other primary design consideration for the CoBox interface(s) are Ledger programme's focus on Human Centric technologies and busineses

> Human Centric puts purpose before profit, puts people before profit, that’s what we mean with it Human Centric means that what we are developing first and foremost useful for the society and then it is sustainable, of course, you can make some money out of it, you can sustain and grow your organisation but it has to benefit the society around you, the people I will say even the living beings, plants and animals included.
> 
> _**Jaromil, CTO of Dyne.org**_ https://vimeo.com/340937562

Lastly in the Architecture Schematics Overview some proposed approaches were; file system mount, system tray server, all in the browser, browser extension, electron application or beaker browser integration. 

This section seeks to outline how CoBox has iterated on these design foci and which of the proposed approaches have then been selected.

##### Process
###### Research Process
As well as building on our previous research on the [usability of peer to peer tools](https://github.com/blockades/mmt_resources/blob/master/research/dark_crystal-report_peer_testing_and_usability_assessment.md) and the [accessibility of cryptographic tools](https://gitlab.com/dark-crystal/research/blob/master/accessibility_of_cryptographic_tools.md) CoBox has attempted to approach the interface research  through two main lenses to support a Human Centric design approach

* Tatiana Mac's [“Building Socially-Inclusive Design Systems”](https://www.youtube.com/watch?v=KxT0EwGrXWU&t=855s) and 
* Una Lee and Dann Toliver's [“Building Consentful Tech"](https://www.andalsotoo.net/2017/10/24/the-building-consentful-tech-zine-is-out/) which itself builds on foundational work by the [Design Justice Network](http://designjusticenetwork.org/zine) and the [Detroit Community Technology Project](https://detroitcommunitytech.org/teachcommtech).

We have summerised these as: 

* Consentful Tech
  * Freely given
  * Reversible
  * Enthusiastic
  * Informed
  * Specific
  * ![consentful tech](./assets/consentful-tech.png)

* Socially-Inclusive Design System
  * Impact > Intent
  * Perpetual Audit
  * Adaptable 

With these lenses as a guide CoBox has had three main vectors of peer facing research, the key findings of which have been highlighted below
* [Engineering Trust - CoBox as a breakup-proof technology](https://ledger-git.dyne.org/CoBox/cobox-resources/raw/branch/master/ledger-deliverables/3_mock-up/assets/cobox_trust_and_thresholds.pdf)
    * ![engineering trust workshop output](./assets/engineering-trust-workshop.png)

* [CoBox in context: An overview of data management concerns in a group of co-operative organisations](https://ledger-git.dyne.org/CoBox/cobox-resources/src/branch/master/ledger-deliverables/3_mock-up/assets/cobox-in-context_need-finding-report-2019.pdf).
    * > Users felt most mastery over the tools they were using when things worked smoothly,and worried most when they did not understand what was going on. Design decisions should unintrusively support awareness of things working well, of users being incontrol of their infrastructure, and not exclusively responsible for that of others. Ie. It should be easy to see that both your data and the data you host is replicated in enough other places to be safe if your building floods.
* Peer Design workshops
    * > ”…just as much about technological solutions as it is about the social tissue that we can nurture-culture, [as a way] to distribute ownership and establish a shared immune system.”

###### Research Summary
Attempts to achieve these human centered design goals by reducing the complexity of the interface whilst highlighting and reenfocing _what is actually happening_. It does so by drawing on the expertise of Magma Collective in onboarding new peers into peer-to-peer systems such as secure-scuttlebutt as well as blockchain projects such as Bitcoin and Ethereum. One of the learnings from these experiences is _what can go wrong_ with the system and how this can overload the social fabric leading to strain and collapse 

On the other hand it is a design aim to communicate the underlying complexity, how this intersects with the principles of consentful tech whilst not overloading context on peers who are new to the system.

##### Implementation Process
* CoBox has taken a component-driven development approach building out components with [storybook](https://www.learnstorybook.com/). The interface has been driven by the research outputs mentioned in the previous section.
* Rapid prototyping with figma to create clickable interfaces to surface required endpoints and do rapid peer A/B testing has then happened.
![figma cobox peer journies](./assets/figma-cobox-peer-journies-screenshot.png)

* Once peer journies have been confirmed we then figured out which API endpoints would be required to be able to support such functionality.
* Frontend framework selected has been VueJS which is popular for its ease of use and establishment - with a balance of sane defaults without locking you into top down design decisions.
* in the Architecture Schematics Overview some proposed approaches were; file system mount, system tray server, all in the browser, browser extension, electron application or beaker browser integration. 
    * We have decided to build for the browser for the MVP. Doing so would be the first step towards building an Electron application so does not close down the possibility that we could persue any of the other routes outlined in the Architecture Schematics Overview
* Interfaces will be intuitive, clear with large script.
![cobox interface font type](./assets/cobox-interface-font-type.png)

##### FUSE (Filesystem in Userspace)

In order to make use of our peer-to-peer file system in a fashion similar to Dropbox, as per our original specification and the mock-up, we created a FUSE mount point for our file system abstraction. Using a regular file system in user space, the CoBox client software eases the process for peers to easily interact with, append, amend and archive files and directories to a group's drive. A key driver for having a simple FUSE file system is user familiarity and it being fundamentally against user-lock-in, another key requirement drawn from our need-finding.

> Build in support for non-technical professional tools that do not force an all-in
approach with locked-in data.
>
> [cobox in context, pp. 2](https://ledger-git.dyne.org/CoBox/cobox-resources/src/branch/master/ledger-deliverables/3_mock-up/assets/cobox-in-context_need-finding-report-2019.pdf)

##### JSON REST API

To make our file system accessible and group application layer visible to users and developers through more complex and user-friendly interfaces than a FUSE mount, we've composed a REST API using the popular javascript web application framework Express. This API opens a series of end-points from which a CLI and UI talk to, in order to switch network sync (replicaton) on and off, to establish scheduling, to mount a group's drive to a given directory, and to read data back from the file system. It also provides an end-point for querying our group's aggregated hypercores that contain peer's application layer data, in order to compile, for example, features as the allow list, a peer's self-defined name in a group, and other application layer data. To do this we've built an intelligent index for running map-filter-reduce queries over a collection of append-only logs.

![Swagger API documentation](https://ledger-git.dyne.org/CoBox/cobox-resources/raw/branch/master/ledger-deliverables/4_MVP/assets/swagger-screenshot.png)
> Our swagger documentation can be found by running [cobox-server](https://ledger-git.dyne.org/CoBox/cobox-server) and visiting http://localhost:3000/api-docs

Our REST API and relevant documentation can be found [here](https://ledger-git.dyne.org/cobox/cobox-server). We've implemented Swagger Docs for clear request/response instruction for interested developers to easily compose their own front-end interfaces on this p2p stack. We've implemented a simple CLI client for creating new groups, joining existing groups, opening a connection on the DHT to sync, mounting a group's file system, for querying a group's file system to render it's history and show remote updates. The CLI also enables starting and stopping of blind replication.

#### 2.4.6 Summary

The features indicated and detailed above bring our MVP to a point whereby our first adopters will be able to experiment with using our technology and we can begin a new phase of feedback and iterative design and development later in 2020. Below is a diagram to add some clarity on how the our different components fit together, and how they communicate using our chosen peer-to-peer protocol, hypercore-protocol.

![cobox-system-diagram](./assets/cobox-system-map.png)

As seen in the diagram, on our client device, cobox-server is run with a spread of different interfaces depending on the client device itself. This will currently work on multiple desktop / laptop platforms. This stores cobox-groups in memory, instances of multifeed that hold an address and an encryption key and enables them to be easily accessible over a locally served web API. Our server performs replication over the hypercore protocol, and talks to cobox-hub, the administration software that runs on our blind replicator device.

### 2.5 Packages

![dependency diagram](https://ledger-git.dyne.org/CoBox/cobox-resources/raw/commit/0d9b46e3e583db66c1280db10bb7e0b3703d1cfd/ledger-deliverables/4_MVP/assets/dependency-diagram.png)
> Dependency diagram

Below is a comprehensive list of the stand-alone libraries we have built since the project's inception.

#### [cobox-ui](https://ledger-git.dyne.org/CoBox/cobox-ui)

A javascript front-end built with VueJS. Talks to cobox-server's REST API.

#### [cobox-cli](https://ledger-git.dyne.org/CoBox/cobox-cli)

A command-line interface build with javascript library Yargs. Talks to cobox-server's  REST API.

#### [cobox-os](https://ledger-git.dyne.org/CoBox/cobox-hub)

A Devuan image builder for use on ARM devices or in VMs. Easy installation and configuration of your cobox device, installing and running cobox-hub.

#### [cobox-constants](https://ledger-git.dyne.org/CoBox/cobox-constants)

System wide constants.

#### [cobox-config](https://ledger-git.dyne.org/CoBox/cobox-config)

Local configuration settings stored in YAML.

#### [cobox-server](https://ledger-git.dyne.org/CoBox/cobox-server)

A JSON HTTP/S REST API to run on the client. Exposes end-points to a front-end to manage your groups and blind replicators, setup cobox devices, and perform admin functionality.

#### [cobox-group-store](https://ledger-git.dyne.org/CoBox/cobox-group-store)

The repository pattern for finding a record based on a dynamic set of parameters from an existing collection. Used to initialize and cache multiple cobox-group instances in a single node process.

#### [cobox-group-base](https://ledger-git.dyne.org/CoBox/cobox-group-base)

A base model for p2p private groups, containing storage and application layer data.

#### [cobox-group](https://ledger-git.dyne.org/CoBox/cobox-group)

Adds content en/decryption and kappa-drive to cobox-group-base model. Provides an additonal feed and indexes for group specific messages, and additional indexes for kappa-drive state / history.

#### [cobox-blind-replicator](https://ledger-git.dyne.org/CoBox/cobox-blind-replicator)

A simple wrapper for cobox-group-base model to initialise multifeed. No content en/decryption so functions as a blind replicator.

#### [cobox-admin-group](https://ledger-git.dyne.org/CoBox/cobox-admin-group)

An RPC implementation over a multifeed instance. Content en/decryption included.

#### [cobox-crypto](https://ledger-git.dyne.org/CoBox/cobox-crypto)

Cryptography primitives used by Cobox. All cryptography is compatible with the existing Dat ecosystem for future extensibility and interoperability.

#### [hypercore-crypto-encoder](https://github.com/ameba23/crypto-encoder)

A content encryption encoder for hypercore using an encryption key.

#### [kappa-drive](https://ledger-git.dyne.org/CoBox/kappa-drive)

Multiwriter peer-to-peer filesystem, built on kappa-core and hyperdrive.

#### [kappa-drive-mount](https://ledger-git.dyne.org/CoBox/kappa-drive-mount)

Mount a kappa-drive using FUSE.

#### [kappa-view-query](https://ledger-git.dyne.org/CoBox/kappa-view-query)

A materialised view for kappa-core, aggregating all feed data in a multifeed instance, serving a dynamic and intuitive map-filter-reduce querying engine.

#### [cobox-hub](https://ledger-git.dyne.org/CoBox/cobox-hub)

Utilises cobox-admin-group with additional setup functionality including UDP packet broacast over a LAN. Enables cobox device administation over both a local or remote connection.

#### [cobox-local-discovery](https://ledger-git.dyne.org/CoBox/cobox-local-discovery)

Find peers on a local network by broadcasting / receiving UDP packets.

#### [cobox-command-schemas](https://ledger-git.dyne.org/CoBox/cobox-command-schemas)

Command message schemas for cobox-hub RPC.

#### [kappa-private](https://ledger-git.dyne.org/CoBox/kappa-private)

Use private-box for sending encrypted messages between kappa-core feeds.

#### [plot-logs](https://ledger-git.dyne.org/CoBox/plot-logs)

To plot a graph of cpu/memory usage from a cobox log file using R.

#### [paper-key-backup](https://ledger-git.dyne.org/CoBox/paper-key-backup)

Export cryptographic keys to an easily printable format.

---

These packages can be laid out in a hierarchical structure to illustrate the stack's dependency tree.

```
cobox-ui
cobox-cli
├── cobox-os
│   └── cobox-hub
│       └──cobox-admin-group
│          └──cobox-group-base
│
└── cobox-server
    ├── cobox-config
    └── cobox-group-store
        │
        ├──cobox-blind-replicator
        │  └──cobox-group-base
        │
        ├──cobox-admin-group
        │  └──cobox-group-base
        │
        └──cobox-group
           └──cobox-group-base
              │
              ├── cobox-crypto
              │   └── hypercore-crypto-encoder
              │
              ├── kappa-core
              ├── multifeed
              │   └── hypercore
              │
              ├── kappa-drive
              │   ├── hyperdrive
              │   │   └── hypercore
              │   ├── corestore
              │   ├── mountable hypertrie
              │   └── leveldb
              │       └── kappa-view-kv
              │
              ├── leveldb
              │   └── kappa-view-query
              │
              ├── kappa-drive-mount
              ├── hyperswarm
              └── hypercore
```

### 2.6 Reflection

Its important to note that while our MVP can deliver a working peer-to-peer cloud storage solution for small collectives and organisations, there remain many core issues at the base of the stack which were beyond the scope of delivering an MVP for CoBox. Many of these were documented extensively in our mock-up document. Our development team strongly believe that further development is needed to build a scalable cloud infrastructure. The core Dat protocol is a popular and actively developed project and will remain so moving into the future. We are in conversation with the key stakeholders in the ecosystem - indeed, we are recognised as one ourselves - and are in dialogue with the core team to collaborate to ensure our future aspirations are aligned and therefore our needs and the needs of CoBox's early adopters and future clients are well met.

## 3. The Future

### 3.1 Future Features

#### 3.1.1 Health Check

Its clear from our own experience, in addition to being ratified by every interview conducted in the research phase and summarised in the need-finding report, that our potential first adopters and clients who currently use cloud infrastructure depend on the stability of those cloud service providers to make it work. Any distributed and democratically organised replacement for corporate cloud services would need to create visibility and ensure transparency on the current state of remote backups. Metrics such as whether they are online, how often they are online, are they staying up-to-date, how regularly do they sync updates, and other important metadata that can provide peace of mind to organisations using a distributed backup mechanism. This is summarised in the statement from the need-finding report:

> "It should be easy to see that both your data and the data you host is replicated in enough other places to be safe if your building floods."
>
> -- [cobox in context, pp. 3](https://ledger-git.dyne.org/CoBox/cobox-resources/src/branch/master/ledger-deliverables/3_mock-up/assets/cobox-in-context_need-finding-report-2019.pdf)

In order to create adequate transparency around an group's remote backups, the team determined that a health check system should be high up the list of features for any roadmap for future development. This would include an intuitive interface rendering up-to-date information about the activities of your blind replicators. Each peer in a group can collect and regularly publish a report on its activities and what it has seen in terms of network traffic. All group members network metadata can then be aggregated locally to produce a common view on the current health of their group's backups.

#### 3.1.2 Scheduling, Bandwidth Limitation and Customisable Replication

Drawing from our need-finding report, there was a clear articulation my multiple interested interviewees for the following feature set:

> * Manage transfers of larger back-ups without crippling network speeds.
>
> * Allow users to allocate bandwidth around their working hours.
>
> -- [cobox in context, pp. 3](https://ledger-git.dyne.org/CoBox/cobox-resources/src/branch/master/ledger-deliverables/3_mock-up/assets/cobox-in-context_need-finding-report-2019.pdf)

Concern about bandwidth usage and the need for scheduling can be interpreted in two different ways, both of which will be useful. From a device administrators perspective, they will want to setup asynchronous time-based processes or cron workers to run on the blind replicator device. From a client devices perspective, similar funcionality would be useful for either a global cron setting for replication of all associated groups, or more granular group-by-group scheduling. This was specified as a deal-breaker in several of our need-finding interviews. As such, while this feature is not core functionality for the MVP, this is a high priority moving forward.

#### 3.1.2 Match-Making and the Registry

![Agreement from mock-up](https://ledger-git.dyne.org/CoBox/cobox-resources/raw/commit/465c3d94077e3db6041a617ba647724efb28e72f/ledger-deliverables/4_MVP/assets/agreement-mockup.png)

In our last MVP document, we clearly articulated both a technical and a business case for establishing a registry of group addresses. The primary role of the registry, likely would be to provide a match-making service or functionality to enable organisations to easily locate partners to fulfil their backup needs. These would be fleshed out in the interface in the form of intuitive statements of need and non-binding / trust-based agreements, examples of which can be viewed in our initial [clickable PDF](https://ledger-git.dyne.org/CoBox/cobox-resources/src/branch/master/ledger-deliverables/3_mock-up/assets/UI-v0.1.0.pdf). On the platform itself, the registry would host requests - such as group's specification of data needs - and offers - detailing a device's storage limitations - for backup, and then provide a group address exchange mechanism to allow device administrators to easily begin blind replication. At the ecosystem level, the health check feature detailed above creates visibility on the implementation of backup agreements. Since improving our specification for distributed authentication, the registry can also act as PKI to ease the process of inviting new peers into groups.

The registry would be setup, either as a service - run by the cobox project to assist with ‘backup match-making’ and expose these addresses for backup - or as a distributed global ledger - hosted by each cobox device which acts as a stable node. Which path the cobox project will take moving forward will depend largely on our business case, as well as the specifications and needs of our early adopters. There are clear privacy considerations to take into account when exploring the possibility of a distributed model, such as global exposure of group addresses.

#### 3.1.3 Improved Authentication

In future, we can extend our authentication approach further, whereby Alice can ask Bob to meet her on the DHT at her public key as the meeting location. Alice and Bob can then conduct a similar authentication handshake, whereby Bob can verify that a peer is Alice, and so Bob can then send Alice the encryption key. Alice can then join the group, and no invite code is exchanged. The beauty of this mechanism is PKI can host peer's public keys. All a group needs to do is find the peer they wish to add, and add them to a group. With future features such as the CoBox registry, a common infrastructure for hosting public keys and providing matchmaking for blind replication, this becomes significantly easier.

An effective distributed authentication mechanism for private groups provides us significant space for further development. By aggregating the allow-list and applying new application logic, such as a threshold signature scheme or Loomio-style poll, we will be able to introduce democratic consent mechanisms internally to groups, to give new members access or to remove existing members from a group. Given Edmund is leaving the organisation and no longer needs access to the group, Clare, Bob and Alice can all agree to revoke access to Edmund's public key, thus ensuring Edmund can no longer replicate the latest changes to the group's file system.

#### 3.1.4 Nested Group Authorization

Sadly due time contraints we have not yet been able to implement an internal authorisation mechanism for groups. However there is a clear roadmap to create 'nested groups', allowing for custom internal permissions. In this case, Alice is a member of Magma 'group'. Magma contains a list of nested group addresses. Bob and Clare are part of an internal working group for Magma's research team, Magma/Research, and are collaborating on a document in their file system. This file system is not visible to regular Magma team members, though they are aware of the working group's existence. Alice has recently joined the research working group and needs to gain access to their file system. Applying the same authentication logic as above, Clare and Bob authenticate Alice, giving her permission by adding her public key to Magma/Research allow-list. Alice has then been authorised to participate in a specific folder. The flow of this process can be smoothed significantly with an intuitive UI, meaning Alice should not need to do anything different, she will simply receive a notification stating she has been given access to a new area in Magma.

#### 3.1.5 Social key backups using Dark Crystal

Social backups allow group members to share responibility for safeguarding keys. Keys can be 'sharded' into a number of components in such a way that each individual share has no information about the original key, yet a specific threshold of components can be combined to recover the key. The implication is that keys can be recovered from other group members following a specified degree of consensus within the group.

![dark crystal](https://gitlab.com/dark-crystal/dark-crystal-protocol-specification/raw/master/assets/dc_shards2.png)

Dark Crystal is a social key management protocol which uses such threshold-based secret sharing techniques. We plan to introduce this key backup and recovery mechanism to Cobox.

#### 3.1.6 Cobox-OS

Cobox-OS is a custom linux operating system designed specifically for Cobox blind replicators. It is designed to run on ARM devices such as the Olimex Lime2, which will be our recommended Cobox hardware, as well as on a virtual machine, and comes with `cobox-hub`, the blind replicator software, pre-installed and ready to automatically run on booting.

Although we have a build script, Cobox-OS has not been extensively tested and needs further work doing.  It should be noted the `cobox-hub` can also be run on other operating systems.

#### 3.1.7 Conflict Resolution with 'Bloom Clocks'

Future development in this area would concern itself with introducing the 'bloom clock', using false positives to infer a partial ordering of events with high confidence.

#### 3.1.8 Multi-Multifeed as a Microservice

Our package cobox-group-store is currently a rudimentary repository pattern for loading and caching multiple groups or multifeed instances into memory from which we can perform queries, operations and other functionality. A much more desirable layout would be to separate our collection of multifeeds into a microservice, with an easy-to-use API to which our core REST API can communicate with using a client package. This would be a notable optimisation of our current setup, enabling more fine-grained process handling with lightweight standalone packages. This would also be a significant contribution to the kappa-db community - enabling developers to begin to use the kappa-core stack intuitively in languages other than NodeJS.

### 3.2 Core Updates

#### 3.2.1 Kappa Core Version 7

After liaising with companion projects using the kappa-db stack and comparing notes, we have spec'ed a planned release of a significant upgrade of the indexing system, Kappa Core, which will provide necessary improvements to our current index setup. The release of v7 is planned for early 2020. Included are breaking changes to its' current API, so a sprint's worth of development will be necessary to integrate the latest upgrades and make the stack production ready.

## References

* [S. Lerner, M. Davarian, J. K. Brekke, _CoBox in context: An overview of data management concerns in a group of co-operative organisations_, 2019](https://ledger-git.dyne.org/CoBox/cobox-resources/src/branch/master/ledger-deliverables/3_mock-up/assets/cobox-in-context_need-finding-report-2019.pdf)
* [A. Enigbokan, D. Hassan, _Engineering Trust: CoBox as a breakup-proof technology_, 2019](https://ledger-git.dyne.org/CoBox/cobox-resources/src/branch/master/ledger-deliverables/3_mock-up/assets/cobox_trust_and_thresholds.pdf)
* [Magma Collective, _LEDGER Work Plan_, 2019](https://ledger-git.dyne.org/CoBox/cobox-resources/src/branch/master/ledger-deliverables/2_work-plan)
* [Magma Collective, _LEDGER Mock Up_, 2019](https://ledger-git.dyne.org/CoBox/cobox-resources/src/branch/master/ledger-deliverables/3_mock-up)
* [J. K. Brekke et al., _Clickable PDF_, 2019](https://ledger-git.dyne.org/CoBox/cobox-resources/src/branch/master/ledger-deliverables/3_mock-up/assets/UI-v0.1.0.pdf)
* [G. Jones, _Dark Crystal: Accessibility of Cryptographic Tools Report_](https://gitlab.com/dark-crystal/research/blob/master/accessibility_of_cryptographic_tools.md)
* [K. Gibb, _Dark Crystal: Peer Testing and Usability Assessment_](https://github.com/blockades/mmt_resources/blob/master/research/dark_crystal-report_peer_testing_and_usability_assessment.md)
* [T. Perrin, _The Noise Protocol Framework_, 2018](http://noiseprotocol.org/noise.html)
* [M. Ogden, K. McKelvey, M. B. Madsen, _Dat - Distributed Dataset Synchronization and Versioning_, 2017](https://github.com/datprotocol/whitepaper/blob/master/dat-paper.pdf)
* [L. Ramabaja, _The Bloom Clock_, 2019](https://arxiv.org/abs/1905.13064)
* [C. Fidge, _Timestamps in Message-Passing Systems That Preserve the Partial Ordering_, 1988](http://zoo.cs.yale.edu/classes/cs426/2012/lab/bib/fidge88timestamps.pdf)
* [D. Keall, _How DAT works_, 2019](https://datprotocol.github.io/how-dat-works/)
* [J. Kreps, _Questioning the Lambda architecture_, 2014](https://www.oreilly.com/radar/questioning-the-lambda-architecture/)
* [U. Lee, D Toliver, _Building Consentful Tech_, 2017](https://www.andalsotoo.net/2017/10/24/the-building-consentful-tech-zine-is-out/)
* [T. Mac, _Building Socially-Inclusive Design Systems_, 2019](https://www.youtube.com/watch?v=KxT0EwGrXWU&t=855s)
