
# Cobox - comparison with market competitors

## Cloud providers

- Remote storage providers, for example Google Drive and Dropbox.
- Business models vary: pay-monthly, free with advertising or data extraction, or free but pay for extra storage/features

Typically encryption is not offered.  Increasing concerns about privacy and data extraction, for organisations who hold data about their clients this is of particular concern.

- [Keybase](https://keybase.io/) - offer encrypted storage with key residing on the user's device, much like cobox. The difference is that keybase provide the encrypted storage themselves, whereas Cobox offers a combination of self-hosting and hosting on a network of partner organisations. This means, that although the keybase company cannot access your data, they can revoke your access to it, for example if they changed their policies or pricing. 
- [Cryptographic Specification of the Keybase filesystem](https://keybase.io/docs/crypto/kbfs)

## Nextcloud

Nextcloud is a self-hosted application platform, with storage syncing. In many cases the user or organisation installs Nextcloud themselves.  There are also providers who offer hosted nextcloud instances.

Installing yourself can be difficult for non-technical users. There are some pre-installed devices available.

- [Nextcloud pre-installed devices](https://nextcloud.com/devices/) - Note that many of these devices are either sold out or no longer available.
- [nextcouldpi](https://ownyourbits.com/nextcloudpi/) Nextcloud preinstalled on a raspberry pi

Nextcloud's storage offers server-side encryption.  This means keys are stored on the host device, rather than the client.  This means you can access your nextcloud files from any device, but so can an attacker who is able compromise the Nextcloud host.

- Business model: Subscriptions for enterprise users, with support and earlier updates.

## NAS - network attached storage

Network Attached Storage devices, such as [Synology](https://www.synology.com/en-us) are a self hosted physical storage device. 

Typically not encrypted, or only server-side encryption (keys reside on the device).  Some companies offer remote backup as an additional service, but generally backup of data stored on the device is done manually by the user.

- Business models typically centered on hardware sales.

# Cobox 

- Client side encryption. Keys reside on the devices of individual group members, the physical cobox devices acts as a blind replicator. 
- Automated distributed backup by partner organisations.
- Physical device comes pre-installed and aims to have a very easy setup process, not requiring a knowledge of network configuration.
