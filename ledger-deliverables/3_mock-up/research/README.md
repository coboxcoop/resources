# Research

## Index

1. [Need Finding Report](../assets/cobox-need-finding-report.pdf)
2. [Soft Thresholds Workshop](../assets/cobox_trust_and_thresholds.pdf)
3. [London Peer Design](../workshops/london-peer-design.md)
4. Other Reports
  * [Accessibility of Cryptographic Tools Report](https://gitlab.com/dark-crystal/research/blob/master/accessibility_of_cryptographic_tools.md)
  * [Peer Testing and Usability Assessment](https://github.com/blockades/mmt_resources/blob/master/research/dark_crystal-report_peer_testing_and_usability_assessment.md)
5. [Peer Stories](./stories)
