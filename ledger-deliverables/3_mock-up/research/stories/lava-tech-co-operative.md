# Professional Backup Services for Lava Tech Co-operative

Lava is a worker owned coop of 10 people providing a full range of digital services to a wide number of clients in the co-operative, SME, NGO sectors. Building new services (web sites and web applications) for clients often requires they set up some cloud based data storage - Digital Ocean, Dropbox or AWS typically. Many of their clients have been asking for alternates to the corporate cloud.

Lava has recently been made aware of the CoBox Stack (CoStack), sees it is free to set up and trial. They want to see if it's feasible to offer cloud storage to clients which they run collectively.

Alex from Lava takes the lead on setting up their CoBox when it arrives (see [The Chicken Coop](./the-chicken-co-op.md) for the setup process).

After running through the set up they elect to list themselves as offering backup services. They then navigate to `http://registry.cobox.coop` and see themselves listed as a coop who will professionally run backups. They also see InLandish on the registry. They click on InLandishs' offer and are taken to the InLandish profile page which indicates how old the coop is, how many members they are, their governance thresholds with a link out to their incorporation documents and byelaws as listed on Companies House and also a DAT mirror of the documents. There is an option to initiate a connection. They do this.

From the perspective of Robyn from InLandish, they have been running their CoBox for a while and have incorporated checking their CoBox interface as part of their inbox workflow. They get an email letting them know that there is a new request to open a connection. Robyn navigates to the admin panel and the connections tab and sees an incoming invite from Alex from Lava Tech Co-operative looking to either pay for storage or do mutual backups. Robyn checks their profile and can see that they are a fellow digital tech co-operative. Given that they have a lot of client work on at present Robyn decides it would be beneficial to do mutual backup given they have alot of extra storage capacity at the moment. This will give InLandish at least 3 backups of all of their data and client data. This makes their backup regime very robust and resilient so it would likely mean they can increase the price of their paid back up option as any data they hold is now backed up by them and three other digital services coops.

Alex from Lava can see that InLandish has accepted the connection and proposed a mutual backup which they agree to by clicking the confirm button. The confirmation asks them to sign that it has been ratified by the threshold number of members. Once signed they can see that they are now being backed up.

