# Setting up Cobox at the Bakery Co-operative

> One of the questions i want to incite with this story, is when a new member joins the group in the initial startup procedure, do they need to first make themselves known to the initiator, or can they simply receive an out-of-band invite. The process below involves more steps, but is safer as it does not require the group members to have an encrypted channel to exchange keys. 

> Also, in this story there is no a physical cobox device. Having one would make the process much simpler, but this illustrates that what is possible without one.

Small bakery workers cooperative of 6 people.  All members work baking as well as sharing administrative tasks. They have a small office at the bakery, but members often do administrative tasks on their computer at home.

They currently use google drive to store files, including the following:
- Constitution documents.
- An archive of their monthly meeting minutes as well as minutes from their annual general meeting.
- Backups of their collaborative spreadsheet of suppliers and customers
- Their invoice templates
- Promotional leaflets and posters in as application files as well as print-ready format.

They decide to join the cobox network, and have found 2 partner organisations which are other cooperatives and existing cobox users, both of which they already know and have personal connections with.

4 members are physically present the day they setup cobox, and have bought their home laptops with them to set up the software so they can use it at home software.  2 members are not working that day, but install the client software from home the following day.

Gertrude is present at the bakery and has installed the cobox software on her computer.  When opening the program, she is asked if she would like to create a new identity or recover an existing one.  She chooses create new identity and gives herself a name. 

Hedwig does the same, and as soon as she does, she and Gertrude see a notification that there is another cobox user on the same local network, but rather than their name, they are displayed as four random english words. 

Gertrude clicks 'create new group' and is prompted to give the group a name.  She then clicks 'add peer to group'. She is given the option to either invite a peer from the local network or to paste in a peer's public key.  Since Hedwig is already connected to the local network, she chooses that option.
She is asked to verbally confi

Hedwig recieves an invite to join the group. After accepting she is able to do the same to invite the other two group members who are also present and connected to the local network.  She then downloads the files from their google drive to her local computer, and uploads them to the cobox by clicking a button in the interface and choosing the folder on her computer containing the files. 

All connected group members see a notification that Hedwig has added some files.

The two members who were not present, Berni and Bob, set up the cobox software at home the following day.  They give themselves a name, but are unable to find anyone on the local network. They click an option to copy their public key to the clipboard and send it to Hedwig by unencrypted email. 

Hedwig pastes the keys from the emails into the 'add peer to group' dialog box. An invitation code is given.  In Berni's case, the cobox software says that berni is currently connected and will recieve the invitation automatically. In bob's case, the software says that since bob is currently not connected, she must either send her the code by other means, or wait until they are both connected at the same time.  Hedwig chooses to paste the invite code into an unencrypted email to bob.

Berni sees a notification that she has an invite to join the bakery cobox, and accepts it an is able to access the files.

Bob recieves the email later and must click on an option in the interface, 'enter invite code'. He enters it, but the interface shows a message which says that there are currently no connected peers in this group to sync with. He leaves his computer switched on for a couple of hours and on returning to it finds he can see and access the files in the interface.

