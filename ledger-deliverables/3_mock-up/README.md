# LEDGER MVP Mock Up ~ Cobox

![cobox-icon](./assets/cobox-icon.png)

## Index
1. [The Story So Far](#1-the-story-so-far)
2. [Revisit: Objectives and Value Proposition for the MVP](#2-revisit-objectives-and-value-proposition-for-the-mvp)
3. [Research](#3-research)
  + 3.1 [Need Finding Report](#31-need-finding-report)
  + 3.2 [Soft Thresholds Workshop](#32-soft-thresholds-workshop)
  + 3.3 [London Peer Design](#33-london-peer-design)
  + 3.3 [Previous Reports](#34-previous-reports)
  + 3.4 [Peer Journeys (user stories)](#35-peer-journeys-user-stories)
4. [Interface Mock Up](#4-interface-mock-up)
5. [Technology](#5-technology)
  + 5.1 [Architecture Schematics Overview](#51-architecture-schematics-overview)
  + 5.2 [Our Modules](#52-our-modules)
6. [The Business Model](#6-the-business-model)

## 1. The Story So Far

Since submitting our MVP work plan, we have been engaged in a rapid process of need finding research, peer design workshops and development of the necessary foundations for a working prototype. This document summarises much of the work we have been engaged in as a team and the outcomes and knowledge that we have gleaned that we will be weaving into further development and research. In respect of the privacy of the groups we are working with, our research summaries will only expose what we have learned without exposing any information about specific practices with regards to systems adminsitration and data backup.

10 co-operatives who work in a wide variety of technical and non-technical areas were interviewed. This need finding research has been compiled into a full report. One of the primary outcomes noticed from this research is that the current descriptions of interdependencies and use of third party services suggests that people already distribute responsibility for the data they deal with. These depend on various forms of existing trust in all cases based on things like personal connections, reputation or contractual arrangements - mapping out those relationships both as recipients and deliverers of those services - and how those networks of trust are constituted could help clarify how mutual backup like CoBox fits into this existing matrix.

Having a file system was something we all felt would be required in all contexts irrespective of the research outcome. The development process has focused on building a functional multi-writer Dropbox-style file system on the peer-to-peer Dat protocol. A preliminary back-end system for reading and writing to files has been implemented while swarming across the Hyperswarm DHT (Distributed Hash Table). A basic user interface was decided on, using FUSE, to mount a drive to an operating system's regular file system, to enable people to easily interact with its content. File content and metadata encryption has been implemented using a shared encryption key, and we are currently running several 'blind replicators', our first prototype cobox device, on several remote computers.

We are in the process of collating different business models and patterns for economic sustainability drawing from experiences in open source and new innovations in DLT.

## 2. Revisit: Objectives and Value proposition for the MVP

### Problem and Solution

#### For Society
* **Problem**: Societal loss of data sovereignty to corporations.
* **Solution**: CoBox brings a cooperative approach to hosting, treating data as a common good owned by citizens. Providing infrastructure to support co-operative sector and beyond to regain data sovereignty.

##### How our MVP tackles this

Our MVP addresses this by offering a distributed and encrypted data hosting model.  Cryptographic techniques mean that rather than pooling data, organisations pool their storage resources so that data can be stored across the network in an encrypted manner. A cooperative approach to storing and backing up data across a distributed network is possible whilst preserving privacy and complying with GDPR.

#### For Technological Ecosystem
* **Problem**: Over dependence on centralised systems, owned by corporations which require an internet connection.
* **Solution**: Offering accessible software and hardware which can support offline first tools for governing and hosting data (as a commons) with the ease of use of corporate software.

##### How our MVP tackles this

Our needfinding interviews showed many organisations had concerns about poor or intermittent internet connections when keeping their files on cloud services.  With Cobox, you can always access data and make changes locally, and these changes are synced with other peers when it becomes possible. Our model also addresses concerns about data-extraction when being dependent on commercial cloud services.

#### For Co-operatives
* **Problem**: Despite extensive global networks of coops there is no equivalent cooperative network for data management. Over reliance on corporate tools which do not match cooperative principles.
* **Solution**: CoBox seeks to build on the historic tendency for networks of coops to collaborate. CoBox leverages advances in modern P2P technologies which enable a technical infrastructure that maps to existing cooperative principles and patterns of governance.

##### How our MVP tackles this

Our MVP addresses this by providing a registry of participating cooperatives and a system of creating agreements between them to share storage resources.

## 3. Research

### 3.1 Need Finding Report

In summary, the interviews suggest that there is strong potential for CoBox to address specific needs related to mutual data back-up in ways that satisfy a widely expressed desire to be more independent of 'big data' companies. A priotity in this effort it to make the shared responsibility low-stress as well as offer tangible advantages, in terms of cost and effort, over aspects of both self-managed independent infrastructure and the corporate cloud.

By working within the existing eco-system and making current interdependencies apparent, CoBox can potentially build a layer of resilience into the current network. A successful approach to the design challenges would need to include a critical people-based requirement, building on existing relationships to plan support for uptake and longevity. The design challenge will also benefit from taking into account more traditional software design considerations, such as easily being able to see the state of back ups etc., as participants placed a high value on dealing personally with known individuals.

Additionally, it became apparent that the succesful switch to open source alternatives to the corporate cloud will more generally depend on key individuals being able to drive this process within an organisation. Whilst the necessity for blind replication in inter-organisation mutual back up was very clear, the interviews also revealed a set of more flexible permissions requirements for file sharing internally and a need for secure options for high trust contexts often with role-based sharing and security being a common requirement.


The full public need finding report can be viewed and downloaded [here](./assets/cobox-in-context_need-finding-report-2019.pdf).

### 3.2 Soft Thresholds Workshop

![soft thresholds workshop](./assets/trust_workshop_sm.png)

This design research is based on accounts of how work projects breakdown and intellectual property is lost in small co-operative organisations. Several weeks of collecting breakup stories culminated in a workshop exploring breakdowns of trust in small co-operative projects. Creating and analyzing the workshop required getting at the social, emotional, and ethical implications underlying the CoBox software/hardware concept. The process was framed by Adeola asking Dan about the goals, objectives and context of CoBox, and the design process, as envisioned by the CoBox engineering team.

- [CoBox Trust and Thresholds workshop](./assets/cobox_trust_and_thresholds.pdf)

### 3.3 London Peer Design

This design session was created by Mu, Jaya and Kieran and conducted in London with four small London-based co-operatives with a variety of technological needs and expertise. The workshop focused on the following:

1. Negative experiences of (data) loss in relationships
2. Positive steps that you feel can be taken to secure your information / data
3. Negative experiences of individual and organisation relationship breakdown

Within each of the contexts, participants were asked to think about what the changing state of trust looked like in their specific stories, how it affected their technical infrastructure, whether the breakdown exposed any acceptable or unacceptable vulnerabilities in that process or exposure of personal information, and what role a permissions system could help or acts as a hindrance. A significant reflection that emerged was that constructing a trust-based network to move off corporate cloud architectures and build more resilient co-operative systems is:

> "...just as much about technological solutions as it is about the social tissue that we can nurture-culture, [as a way] to distribute ownership and establish a shared immune system."

### 3.4 Previous Reports

#### Accessibility of Cryptographic Tools Report

This report was published as part of our research for Dark-Crystal key backup system. It exposes the reasons why people are slow to adopt cryptographic tools. By using a key backup and recovery system, Cobox is able to offer something new in terms of maintaining privacy whilst offering distributed data storage.

- [Accessibility of Cryptographic Tools Report](https://gitlab.com/dark-crystal/research/blob/master/accessibility_of_cryptographic_tools.md)

#### Peer Testing and Usability Assessment

Peer testing was previously undertaken by members of the CoBox team through individual and group sessions at multiple meetups and events across a period of two months. Much of the feedback opinions and data was collected at the Simply Secure design residency in Berlin in January 2019, at Agorama Sever Coop in London in February 2019 and at Art Hack in Wellington, NZ in January 2019.

- [Peer Testing and Usability Assessment](https://github.com/blockades/mmt_resources/blob/master/research/dark_crystal-report_peer_testing_and_usability_assessment.md)

### 3.5 Peer Journeys (user stories)

Some [example peer journeys](./research) for Cobox:

* [Setup at the Local Bakery Co-operative](./research/stories/bakery-co-operative.md)
* ['The Chicken Coop' Housing Co-operative](./research/stories/the-chicken-co-op.md)
* [Professional Backup Services for Lava Tech Co-operative](./research/stories/lava-tech-co-operative.md)

## 4. Interface Mock Up

![UI mock-up screenshot](./assets/UI_screenshotsm.png)

Walk-through the on-boarding experience of a new peer joining the Cobox network

* [Clickable PDF](./assets/UI-v0.1.0.pdf)

## 5. Technology

### 5.1 Architecture Schematics Overview

![Client-endpoints-thumbnail](./assets/client-end-points_sm.png)

An overview of the technical architecture. Addressing the particular challenges we face(d):
1. Blind replication as a backup procedure
2. A peer-to-peer shared / multi-writer file system over the hypercore protocol
3. Connecting peers for blind replication

* [Overview of technical architecture](./technology/architecture.md)

### 5.2 Our Modules

![Dependency diagram thumbnail](./assets/dependencies_sm.png)

A map of the modules we've built so far, and what we have to build going forward.

* [Descriptions of the modules](./technology/modules.md)

## 6. The Business Model

While on the one hand traditional business models might focus on the sales proposition of new products and services, platform economies and DLT entail designing markets and economic spaces rather than a product or service.

In the realm of platforms, this has led to business models that turn the activity and users of the platform itself into the product to be sold for advertisement or intelligence purposes. The development of cryptocurrencies and DLT has to some degree been a protest and response to this both technically and economically. The ambition has been to firstly make such surveillance based business models technically impossible (through privacy by design), while opening up for new types of economic arrangements and possibilities for the longer term economic sustainability of a network and the communities that use these.

CoBox looks to take advantage of the thinking in this realm to not immediately assume a product or a service, but rather to understand how the full eco-system of people and organisations that would deploy, support, make use of and engage with CoBox might contribute to the long term support and wellbeing of the network - both social and infrastructural.

For example, in order for CoBox to work, it needs to not only support cooperation for data storage on a technical level but also enable groups and organisations to form agreements of how they want to handle their mutual backups, what they agree to do in case that relationship ends, and, importantly, where to go for support: 
- technical support
- governance support
- legal support

Thinking of CoBox as enabling connections of different kinds, one kind of relationship that we would look to facilitate is exactly such support roles. In particular, to connect data protection officers with groups that might need assistance in terms of legal and technical obligations. 


- [A comparison between Cobox and alternative solutions on the market](./business/comparison.md)

- [Workplan Business Plan Layout](https://github.com/coboxcoop/cobox-resources/tree/master/ledger-deliverables/2_work-plan#35-business-plan-layout)


## 7. Demo

- [Command Line Tool - Instructions and Demonstration](./technology/cli-demo.md)
