# Architecture Schematics Overview - Cobox

- [Our modules so far](https://ledger-git.dyne.org/CoBox)
- [The DAT project](https://dat.foundation/)
- [KappaDB](https://github.com/kappa-db)

## Web 2.0 meets Web 3.0

![](../assets/client-end-points.png)

This diagram shows how a single client device interacts three separate core components:

1. with the local storage of their cobox groups and their equivalent file systems
2. with the cobox device, initially over their local network, then over the hypercore protocol for executing commands to back-up other's groups
3. with the cobox registry, a service run by the cobox project to assist with 'backup match-making' and expose addresses for backup

This diagram illustrates how clients devices communicate with both the cobox device itself, and with each other in a given group context. This is done over the hypercore protocol across the hyperswarmm DHT.

![](../assets/cobox-system-map.png)

## The 'Group' - Feeds for Each Peer

![feed diagram](../assets/feed-diagram.png)

This diagram shows the different feeds a peer has. Each feed is an append only log. 

`hyperdrive`, gives us a single-writer file system abstraction from two feeds. `kappa-drive`, adds an additional 'state' feed, which is given a new log entry every time a change is made to a file. The materialised views, collate the relevant feeds for all peers into a single database.  The state feeds are collated into a key-value store, indexed by filename, which tells us which peer's hyperdrive contains the most recent state of a particular file. In some cases, we may have multiple 'current states', and we need to use conflict resolution, described below, to decide which state to accept.

`cobox-group` adds an additional feed with application-level data, described below. These are indexed such that we can write structured queries to find particular information.

### Distributed File System

A file store for the group which is resilient to the loss of any one device.

- [kappa-drive](https://ledger-git.dyne.org/CoBox/kappa-drive) A multiwriter peer-to-peer filesystem, built on kappa-core and hyperdrive, originally written by Karissa McKelvey as [peerfs](https://github.com/karissa/peerfs).  Hyperdrive gives us a file system abstraction based on append only logs.  Kappa-drive extends this to represent a set of hyperdrives as a single file repository. Changes to the filesystem are represented by entries in the logs of individual peers, and there is no single source of truth or single location where the files reside.

#### Noteworthy Dependencies

- [hyperdrive 10](https://github.com/mafintosh/hyperdrive/tree/v10), a real time distributed filesystem.  Version 10 includes substantial changes which we rely on. It is now API-complete but still published as 'pre-release'.
- [kappa-core](https://github.com/kappa-db/kappa-core) is a system for creating materialised views over a set of append only-logs. [Cabal](https://cabal.chat/) is a IRC-like chat platform build on kappa-core.
- [multifeed](https://github.com/kappa-db/multifeed) - the underlying mechanism of kappa-core - a set of 'hypercores' with a multiplexer for replicating them as a single stream. 
- [hypercore](https://github.com/mafintosh/hypercore) - the underlying data structure used by both multifeed and hyperdrive. An append only log, where  signed merkle trees are used to verify its integrity.
- [kappa-view-kv](https://github.com/kappa-db/kappa-view-kv) a key-value store on top of kappa-core

### Kappa Architecture

With traditional databases, everyone reads and writes from a dataset which is the single source of truth. With kappa architecture, the source of truth is represented by a materialised view generated from a set of append-only logs. Anybody can write data to their own log, but the state of the database is determined by conventions in how sets of these logs are interpreted. 

### Conflict Resolution

This is a major issue in multi-writer distributed systems.  How to resolve conflicts when two peers make changes which 'clobber' each other (for example, making conflicting changes to the same file)?

When conflict's arise, all possible states are stored in the underlying data structure, and kappa-drive needs to decide which is the correct state to present to the peer. In a perfect world, where each peer is constantly connected to the network and all peers have synchronised clocks, we could simply use timestamps to derive the most up-to-date state of the repository.

Unfortuanately numerous peer-to-peer projects have revealed that relying on timestamps alone can be unreliable.  Due to connectivity issues, we are often more interested in the changes made which **build upon previous changes by others** than on the newest changes in terms of time. 

['Vector clocks'](https://en.wikipedia.org/wiki/Vector_clock) are a method of ordering events based on causality - it is amount of knowledge of previous events which makes a particular peer's change more likely to be at the 'newest' position in the vector clock. Cobox has several conflict resolution techniques available which make use of vector clocks, and timestamps are used as a tie-breaker.

### Blind Replication

![blind replicator view](../assets/blind.png)

Blind replication refers to being able to replicate a data set without being able to read it's contents. This allows a group to create a dynamic backup of another group's data without being able to read it themselves, thus providing backup storage without needing to take responsibility for holding the content. This should be combined with a key management technique so that the encrypted data can be retrieved in the case of data loss.

This is an important feature for Cobox as it allows affiliated co-operatives to backup each other's data, following the principle of mutual aid.

- [cobox-crypto](https://ledger-git.dyne.org/CoBox/cobox-crypto) cryptographic operations for cobox, including a hypercore-compatible encryption encoder.
- [sodium-native](https://github.com/sodium-friends/sodium-native) - javascript bindings to the underlying C library, libsodium.

Blind replication is done by encrypting all content in a hyperdrive / hypercore instance using a 'shared secret' - a symmetric encryption key. Read access to the repository is achieved through replicating across a p2p network using the public key, and decrypting the hypercore logs locally using the encryption key. This can be shared with others over a secure connection by concatenating the public key with the shared secret.

### Key Management

Issues with key management are another major problem in distributed systems. What happens when someone looses a device? Can access be revoked if a key is compromised? Is it possible to restore an account or retrieve old messages when a key is lost?

Cobox will provide social key backup and recovery using 'dark crystal', a system for social key recovery designed for peer-to-peer networks.

- [dark crystal](https://darkcrystal.pw/) Social backups using threshold-based secret sharing
- [research for dark crystal](https://gitlab.com/dark-crystal/research)

### Application Level Data

We have added an additional 'log' feed for application-level data. Currently this is used for publishing 'about' messages where a peer can assert identifying information about themselves, such as a name and an avatar image. We plan to use this feed for other types of application-level information such as 'shares' for key recovery, notifications, and messages between peers.

- [kappa-view-query](https://ledger-git.dyne.org/CoBox/kappa-view-query) - a map-filter-reduce querying engine for kappa-core
- [kappa-private](https://github.com/coboxcoop/kappa-private) - an encoder allowing peers to send private messages to each other

### Permissions, Private Groups, Revoking Access

Private groups are a difficult problem in cryptography. Making existing material available to a new member, or revoking access from a member who has left the group.

As a horizontal governance tool we want a way of managing group members which does not give an imbalanced power dynamic.

- [decentstack](https://github.com/decentstack/decentstack) (formerly 'replic8') will provide pluggable middleware for replication, allowing us to write 'replication policies' giving more control over who has access to which feeds.

### FUSE File System Mount

Kappa-drive can be mounted locally as a directory on the peer's device, using FUSE. This means they can access files in a cobox using their usual applications and file-manager, but behind the scenes, each file system operation is translated into queries and writes for kappa-drive. 

- [kappa-drive-mount](https://ledger-git.dyne.org/CoBox/kappa-drive-mount) - our module providing FUSE mounting
- [hyperdrive-fuse](https://github.com/andrewosh/hyperdrive-fuse) - provides FUSE mounting of an individual hyperdrive 
- [fuse-native](https://github.com/fuse-friends/fuse-native) - the underlying javascript bindings to FUSE 

### Peer discovery

The process by which peers find each other on the network.

- [discovery-swarm](https://github.com/mafintosh/discovery-swarm) The peer discovery system Cobox. Uses the Bittorrent DHT, centralized DNS servers run by the DAT foundation and Multicast DNS simultaneously.  In practice, the centralised services work so well that the DHT is rarely used, meaning it is not a genuinely decentralised solution, but it works well.
- [hyperswarm](https://github.com/hyperswarm) - 'a distributed networking stack for discovering peers' consisting of a DHT which uses 'hole punching' for NAT traversal.  Cobox also has support for hyperswarm implemented, but it is not enabled by default.
- [Tor DAM](https://github.com/parazyd/tor-dam) a peer discovery mechanism using the TOR network which would also provide anonymity.


### Potential performance bottlenecks

![memory graph](../assets/line_chart.jpg)

Events, errors, and CPU and memory usage are logged to a file. Here is an early graphic representation of memory usage over time.

We have not yet conducted scale/load testing.  But are aware of potential bottlenecks these are:

- Large amount of content - giving storage issues to individual peers. Solution is to use sparse replication of the content feeds, trade-off is consistent availability of content.
- Large amounts of changes to files. This makes indexing more memory-intensive.
- Large amounts of peers. This makes replication more memory-intensive.

### Offline-first

Kappa-drive continues to function when offline, or with only partial connectivity with the rest of the group. Any changes made are replicated as soon as a connection is re-established, and peers replicate the data of other peers in a similar way to how bittorrent peers download data off each other rather than the original source.

### NAT Traversal

Another major difficulty with peer-to-peer systems is NAT traversal. How can you communicate directly with a peer who is behind a Network Address Translator? Port forwarding is one option, but ideally peers should not need to reconfigure their router (as this can be difficult for non-technical people and not everybody has access to their router). Universal plug and play (uPnP) is another option but it has security issues. 

### De-Duplication

This is another big problem in distributed systems - how to optimise disk usage by avoiding that the same data is stored more than once. The extent to which we want to prioritise this relates to the use-cases we are catering for - do we want to account for large amounts of media files.

DAT's hyperdrive stores data in chunks, but changes to a file are represented by a complete copy of the updated file, which means a large file with many small amendments uses a very large amount of space. 

Furthermore, there is no strategy for de-duplication between hypercores, meaning that if there are several hyperdrives which contain many files in common, these files will be stored several times. 

### Sparse Replication

Linked to the storage concerns outlined above, sparse replication would allow peers to only replicate the parts of data they are interested in. 

### Decision making tools

An important tool for horizontal governance is a way of recording opinions to come to a decision.

- [loomio](https://www.loomio.org/)
- [ssb-poll](https://github.com/ssbc/ssb-poll-schema)

### Hardware

![A20-OLinuXino-LIME2](../assets/A20-OLinuXino-LIME2.jpg)

- [A20-OLinuXino-LIME2](https://www.olimex.com/Products/OLinuXino/A20/A20-OLinuXino-LIME2/open-source-hardware) Open source hardware, ARM Linux single board computer 

