# Command line demo

This demo shows the parts of cobox which are already implemented. There is currently not graphical user interface, you currently join a cobox group by exchanging keys manually and pasting them into a command line client, as described here.

## 1. cobox-group single group CLI
![screenshot](../assets/cobox-demo-screenshot.png)

This demo shows the separate command line client interacting with a local server that manages joining, mounting, unmounting and syncing the latest changes, to each group's file system. There is an easy to access display of the groups and the shared encryption keys they use.

### Setup

#### 1. Install correct version of NodeJS

* Follow 'Install & Update script' instructions for Node Version Manager.
* https://github.com/nvm-sh/nvm

For this, you will need to have **either** `curl` or `wget` installed. It is likely that you already do.  Try doing `curl --version` or `wget --version`. If you don't have either, you will need to install one of them with your package manager. For example on debian or ubuntu: `sudo apt install curl`

To install nvm using curl:
`curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash`

**or** To install nvm using wget:

`wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash`

**then**... restart your terminal or open a new one

Check if nvm is installed by typing `nvm`.  You should see usage information.  If you do not, close your terminal and open a new terminal window and try again.

* Install node v11.12.0 -- `nvm install v11.12.0`
* Check that node has installed correctly by doing `node --version`

#### 2. Install cobox-group

* Install the command line client -- `npm i -g cobox-group`

#### 3. How to use the cobox-group command line client

You should now be able to run the following command, `cobox --help`, and see something like this:

```
    Usage:
      /shared/Projects/cobox/cobox-group/bin <key> - join an existing cobox
      /shared/Projects/cobox/cobox-group/bin <name> - load a named cobox from config or create one with the given name
    Options:
        --storage <directory> where to store the configuration (defaults to ~/.cobox)
        --mount <directory> where to mount the drive (defaults to './mnt')
        --hyperswarm  use hyperswarm for peer discovery instead of discovery swarm

```

To setup a new cobox drive in your home directory, choose a name for the group, and run the following: `cobox <name> --mount ~/cobox/<name>`.

You should then see something like this:

```
Creating new group with name <name>
Access key is f6c9d819938395c22d2556bb455317e5da69e0cd4e6a7277175ee0c647cc634ea1c135c13077137a14c7b2d462c1cc8426302b6b5f0ac3410592c73509dfc293
Blind replication key is f6c9d819938395c22d2556bb455317e5da69e0cd4e6a7277175ee0c647cc634e
[SWARM] on 9c03510815eadd3720278792b2f02fcdc34393fb8955705f4567735c3b4f2a1e
Mounted group drive at '/home/USER/cobox/name'. To unmount and exit, press Ctrl + C twice.
```

Leave this terminal window open.

You should now, either using your file manager, or another terminal window, be able to see a `cobox/<name>` directory.  Drag some files in, see if you can open them, change them. See what happens!

---

#### 4. Lets try it for real!

Access key: 
```
9e278ff049a84c5cb64056961266fadc91ebccfa68472cf6307afff1f150e946e5c33a14ee3ba9f75c832a34f2e810b0728e248807876747dcffa376849b4072
```

Blind key:
```
9e278ff049a84c5cb64056961266fadc91ebccfa68472cf6307afff1f150e946
```

Command to run...
```
cobox 9e278ff049a84c5cb64056961266fadc91ebccfa68472cf6307afff1f150e946e5c33a14ee3ba9f75c832a34f2e810b0728e248807876747dcffa376849b4072
```

The archive will now be accessible at the directory `mnt`. As soon as you see a `[PEER]` message in the console output, you have replicated with another peer and the archive will be updated with changes they have made.

---

Lets break this down, because we're dealing with many keys here...

**Blind Key** - This is the 'address' or the 'public key'. If we share this, another device can replicate our data from us, but they can't decrypt the content.

**Access Key** - This is the 'address' or the 'public key', _plus_ the 'encryption key', merged into a single long set of characters. We can use this to **read**. There is a separate key for **writing**.

If you want to see more information about your configuration, what keys you're storing and where, run this command: `cat ~/.cobox/config.yml`. You should see something like this:

```
groups:
  byKey:
    8f632b779e3a9ee51f9187dd77df11304f69530a2f29962312357f1f6567fd8e: &ref_0
      encryptionKey: 2794a32df13a1b29f6a41ece54c6fa7f2dc6bc15e6fca1c3370cc6f17ef40776
      name: bark
      publicKey: 8f632b779e3a9ee51f9187dd77df11304f69530a2f29962312357f1f6567fd8e
      symmetricKey: aaa89ea0a8e0f20923b0d108e777cad0e28f615d779f750ee0216b13c98f579a
  byName:
    bark: *ref_0
``` 

* At the moment, I've got a group saved, called `bark`
* Which has a public key / address `8f632b779e3a9ee51f9187dd77df11304f69530a2f29962312357f1f6567fd8e`
* The key used to encrypt messages is `2794a32df13a1b29f6a41ece54c6fa7f2dc6bc15e6fca1c3370cc6f17ef40776`
* Which is derived from the symmetric key `aaa89ea0a8e0f20923b0d108e777cad0e28f615d779f750ee0216b13c98f579a`.

> **NOTE**: We haven't handled custom storage of our 'write' key(s) yet. These are the normal key generated by hypercore / hyperdrive (a.k.a. Dat), and are currently stored at `~/.cobox/groups/<address>/<your-personal-feed>/secret_key`. We will store these at an easier to access location so we don't end up having a big stack of keys in wierd places. I'm hoping we can find a way to make is so we can only have _one_ write key, not multiple, but thats for later...

---

## 2. cobox-server / cobox-cli multiple groups CLI
![screenshot](../assets/server-cli-demo-screenshot.png)

### Setup

#### 1. Install NodeJS and Git

* See above.

#### 2. Download cobox-server and cobox-cli

```
git clone https://ledger-git.dyne.org/CoBox/cobox-server
git clone https://ledger-git.dyne.org/CoBox/cobox-cli
```


#### 3. Start the server

```
cd cobox-server
npm start
```

#### 3. Use the CLI

```
cd cobox-cli

node bin groups list

node bin groups join --name magma

node bin groups list

node bin groups mount --name magma

node bin groups sync --name magma

node bin groups unmount --name magma
```

---

## Troubleshooting

Check you have fuse installed:
`fusermount --version`

debian/ubuntu package: 
`sudo apt install libfuse-dev`

CentOS / Redhat: 
`yum install fuse-devel`

arch:
`fuse-common`, `fuse3`, `fuse2`

Unmounting a drive manually:
`fusermount -u <directory>`

If having mount problems and fuse is definately installed, try:
`sudo mount -t fusectl fusectl /sys/fs/fuse/connections`

## Feedback

Please create issues on Gitea, and fill out this as best you can! The more context we get, the more likely we are to be able to figure out what the issue is. So if you can take a few minutes to try to work out what happened, that'd be super helpful.

https://ledger-git.dyne.org/CoBox/cobox-group/issues

```
<!--- Provide a general summary of the issue in the Title above -->

## Expected Behavior
<!--- Tell us what should happen -->

## Current Behavior
<!--- Tell us what happens instead of the expected behavior -->

## Possible Solution
<!--- Not obligatory, but suggest a fix/reason for the bug, -->

## Steps to Reproduce
<!--- Provide a link to a live example, or an unambiguous set of steps to -->
<!--- reproduce this bug. Include code to reproduce, if relevant -->
1.
2.
3.
4.

## Context (Environment)
<!--- How has this issue affected you? What are you trying to accomplish? -->
<!--- Providing context helps us come up with a solution that is most useful in the real world -->

<!--- Provide a general summary of the issue in the Title above -->

## Detailed Description
<!--- Provide a detailed description of the change or addition you are proposing -->

## Possible Implementation
<!--- Not obligatory, but suggest an idea for implementing addition or change -->
```

### Things to think about

* Installation - is it easy?
* Names for keys - Read / Write / Encryption / Address / PublicKey / Blind Key / Access Key - what do they mean to you?
* Interface

### Problems

### Worked well

### Ideas/nice-to-haves

- Option in the client for 'max-size' - a size when reached it will stop replicating. (and maybe a sane default value)
- Convention for referencing a particular file inside a cobox (eg: to send to someone in a chat)
