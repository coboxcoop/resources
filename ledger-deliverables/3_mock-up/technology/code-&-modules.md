# Code & Modules

## Dependency diagram

![dependency diagram](../assets/dependencies.png)

## Overall features

* Private group encryption
* Blind replication for remote distributed backup
* A shared file system
* An intuitive and accessible interface

## Our Libraries

* Available on [Ledger Gittea](https://ledger-git.dyne.org/CoBox)

The following are all NodeJS modules, beginning with low-level primitives and finishing with front-end interface.

### [cobox-crypto](https://ledger-git.dyne.org/CoBox/cobox-crypto)

Cryptography primitives used by Cobox.

#### Considerations:
* All cryptography should aim to be compatible with the existing Dat ecosystem for future extensibility and interoperability

#### Deliverables:
* [x] a symmetric key generator for creating shared secrets
* [x] an `ed25519` asymmetric keypair generator for creating identities
* [x] a hypercore compatible, crypto encoder encrypting file system changes and application layer data for groups
* [x] generate a `READ_KEY` for sharing, enables read and write permissions in a cobox group: `concat(pubKey, sharedSecret)`
* [x] generate an `ENCRYPTION_KEY` for write permission in a cobox group: `hash(pubKey, sharedSecret)`

### [kappa-drive / peerfs](https://ledger-git.dyne.org/CoBox/kappa-drive)

Multiwriter peer-to-peer filesystem, built on kappa-core and hyperdrive.

- Has an API similar to that of a single `hyperdrive`.  Which in turn is modelled on `fs`. So you use the same methods you would use to access normal files.
- All methods implemented which are used by `hyperdrive-fuse`, allowing for mounting using `kappa-drive-mount`.
- Protocol buffers used to encode the state feed.
- Materialised view of state feed, giving a key-value representation of the state of the archive, indexed by filename.
- Unit tests and coverage.

### [cobox-group](https://ledger-git.dyne.org/CoBox/cobox-group)

A single p2p private group, containing storage and application layer data.

- Adds content encryption to a kappa-drive using our cryptography scheme.
- Enables blind replication when the address / public key is shared, but not the encryption key.
- Additonal personal feed for publishing application layer logic, intially a username.
- Additonal indexer and querying interface `kappa-view-query` for aggregating all peer's personal feeds into data streams.
- A minimal CLI for joining new or existing groups, syncing to the network and mounting as a file system.

### [cobox-config](https://ledger-git.dyne.org/CoBox/cobox-config)

Store and retrieve configuration for cobox from a yaml file.  Keys for each group are stored in this file.

### [cobox-server](https://ledger-git.dyne.org/CoBox/cobox-server)

Exposes a JSON HTTP API, for interacting with multiple cobox groups in a single node process.

* API end-points for listing all groups, joining, mounting, unmounting and syncing individual groups.

### [kappa-drive-mount](https://ledger-git.dyne.org/CoBox/kappa-drive-mount)

Mount a kappa-drive using FUSE.

- Unit tests simulating a range of possible multi-user scenarios.

### [cobox-cli](https://ledger-git.dyne.org/CoBox/cobox-cli)

Command line interface to cobox-server's API.

* A set of commands to list, join, mount, unmount and sync multiple cobox groups in a single node process.

### [kappa-view-query](https://ledger-git.dyne.org/CoBox/kappa-view-query)

A middleware for kappa-core providing an open-ended map-filter-reduce querying engine, aggregating all feeds in a multifeed instance. 

### [kappa-private](https://ledger-git.dyne.org/CoBox/kappa-private)

Use private-box for sending encrypted messages between kappa-core feeds

### [plot-logs](https://ledger-git.dyne.org/CoBox/plot-logs)

To plot a graph of cpu/memory usage from a cobox log file using R

## Our Dependencies

```
cobox-ui
cobox-cli
└── cobox-server
    ├── cobox-group
    │   ├── multifeed
    │   │   └── hypercore
    │   ├── kappa-drive
    │   │   ├── hyperdrive
    │   │   │   └── hypercore
    │   │   ├── corestore
    │   │   ├── mountable hypertrie
    │   │   └── leveldb
    │   ├── kappa-view-query
    │   │   └── leveldb
    │   └── cobox-crypto
    ├── cobox-config
    ├── kappa-drive-mount
    └── hyperswarm / discovery swarm
```

## [multifeed](https://github.com/kappa-db/multifeed)
* multiple hypercore manager
* multiplexer

## [decentstack](https://github.com/decentstack/decentstack)
* kappa-architecture application framework
* enables custom replication policies for authentication

## [hypercore](https://github.com/mafintosh/hypercore)
* append only log

## [hyperdrive](https://github.com/mafintosh/hyperdrive)
* file system operations on top of two hypercores
* 'metadata' and 'content' hypercores

## [corestore](https://github.com/andrewosh/corestore)
* hypercore manager and replicator
* required for hyperdrive v10
* (would like to transition to using this instead of multifeed)

## [mountable-hypertrie](https://github.com/andrewosh/mountable-hypertrie)
* distributed k/v store
* required for hyperdrive v10
* allows for hyperdrives to be mounted inside eachother
* (would like to transition to using this for determining shared file state in kappa-drive)
* (would like to use this to nest kappa-drives inside eachother)

## [hyperswarm](https://github.com/hyperswarm/hyperswarm) / [discovery-swarm](https://github.com/mafintosh/discovery-swarm)
* replication via DHT connection or Dat network-bootstrap server
