# Technology

## Index
1. [Architecture Schematics Overview](.//architecture.md)
2. [Code & Modules](.//modules.md)
3. [CLI instructions and demonstration](./cli-demo.md)
