
## Setting up the lime2

You need a micro SD card reader for your computer (or an adaptor for a normal sized SD card if you have a computer with an integrated SD card reader).

You need a copy of Armbian Bionic for the lime2, available [here](https://www.armbian.com/olimex-lime-2/)

## Step one - flash Armbian Image onto SD card

- Connect the SD card
- type `lsblk`.  You need to determine which device is your SD card, which will be `sd` followed by a letter, for example `sdc`.  If it is not obvious from the output of `lsblk`, try removing the card, doing `lsblk` again, and noting which device is missing.
- Unmount the SD card. eg: `umount /dev/sdc1` (where `sdc1` is the mounted partition).
- Copy the Armbian image onto the card: 
  - `sudo dd if=Armbian_19.11.6_Lime2_bionic_current_5.4.8.img of=/dev/sdc bs=4M`
  - replace with the path to your image file and the correct device letter. 
  - NOTE: if you flash to the wrong device, you could irreversibly delete all data on your computers hard disk. So double check that you are copying to the correct device.
  - NOTE: partition number not needed (eg: `/dev/sdc`, not `/dev/sdc1`)
  - This command may take a few minutes to complete.
- When this is done, the SD card may automatically mount.  If it does not, you can remove and re-insert it, or manually mount it, eg: `mount /dev/sdc1 some-directory`
- Check that the Armbian files are on the card, there should be several directories.
- Unmount and remove the SD card, and insert into the Lime2

## Step two - setup Armbian
- Connect power cable, and ethernet cable to a router.
- Wait a few moments
- If you can log into your router, you should easily be able to see that the Lime2 has a DHCP lease, and find its IP on the local network.
- Otherwise you may need to use a scanning tool such as [nmap](https://nmap.org/). Eg: if your local network is in the IP address range `192.168.1.0/24`, you can do `nmap 192.168.1.0/24`
- Connect via ssh as root. Eg: `ssh root@<ipoflime2>`
- Use the default password, `1234`.
- Armbian will get you to set a new root password and help you set up a second user account.
- End the ssh session (`exit`), and reconnect with your non-root user account. `sudo ssh myusername@<ipoflime2>`

## Update Armbian and install software
I have written a [script to do this](./install-olimex.sh), but i recommend entering the commands manually as it is easier to resolve problems.
- Update OS - `sudo apt update && apt upgrade`
- Install some dependencies of cobox - `sudo apt install python libtool`
- Install Node version manager - `curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.2/install.sh | bash`
- Update shell with settings from nvm - `source ~/.bashrc`
- Install correct node version `nvm install 11.15.0`
- Install cobox hub `npm i -g cobox-hub`

## Set up an external disk
- Check which devices already exist, for comparison later, eg: `lsblk`.
- Switch off Olimex, eg: `sudo halt`
- Connect the SATA and power cables from the lime2 to the disk.
- Switch on the lime2 (either with power button or by reconnecting power cable)
- Reconnect to the lime2 via ssh.
- Identify which device is the disks primary partition (eg: /dev/sdb1)
- If the disk is not formatted, format to `ext4`. How to do this is not covered in this document.
- Create a directory where you would like to mount the disk, eg: `sudo mkdir /data`
- Find the UUID of the disk, eg: `sudo blkid`, and copy it to the clipboard.
- Add the disk to `/etc/fstab` using a text editor, eg: `sudo vim /etc/fstab` and add the following line:
- `UUID=<UUID of new disk>   <mountdirectory>   ext4   defaults,noatime,nodiratime  0 2`
- Reboot the device. `sudo reboot`
- Reconnect via ssh, and verify that the disk is mounted. Eg: `df -h` should show you the disk and the available space on it.
- Configure cobox-hub to use a storage directory located on the external disk.
