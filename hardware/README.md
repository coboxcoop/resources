
## Hardware Product

Since cobox is a 100% open source project, we are using open source hardware, and even our case design is going to be released under the general public license.

- [Hardware components list](./components-list.md)
- [Guide to setting up the lime2 for cobox](./setting-up-lime2.md)

### ARM Computer

![olimex lime2](https://www.olimex.com/Products/OLinuXino/A20/A20-OLinuXino-LIME2/images/thumbs/310x230/A20-OLinuXino-LIME2.jpg)

We chose to use the [Olimex Lime2 ARM computer](https://www.olimex.com/Products/OLinuXino/A20/A20-OLinuXino-LIME2/open-source-hardware), because: 
1. It is open source hardware, so can be audited for security.  This was an absolute must.
2. It scored best in the [Decode project one-board-computer benchmark comparisons](https://decodeproject.eu/publications/benchmark-and-comparison-one-board-computers-and-hardware-specifications).
3. It has an on-board SATA connector which is important as this is a storage-oriented project.

## Enclosure design

There is a case already available for the Lime2, but it does not fit a hard-disk in as well.  Furthermore, we wanted a case which has an aesthetic which fits our product, so we decided to design one from scratch.

Following a consultation on materials and techniques with maker [Rutger Oomkes](https://waag.org/en/rutger-oomkes) from the Waag Creative Care Lab, we decided to go with a 3D printed case.

The enclosure was designed by mechanical engineer [Mario Voigt](https://fablabchemnitz.de/mario-voigt/) and the first prototype 3D printed at [Fablab Chemnitz](https://fablabchemnitz.de/).

![enclosure1](https://ledger-git.dyne.org/CoBox/cobox-resources/raw/branch/master/hardware/assets/enclosure1.jpg)

![enclosure2](https://ledger-git.dyne.org/CoBox/cobox-resources/raw/branch/master/hardware/assets/enclosure2.jpg)

![prototype1](https://ledger-git.dyne.org/CoBox/cobox-resources/raw/branch/master/hardware/assets/prototype1.jpg)

![prototype2](https://ledger-git.dyne.org/CoBox/cobox-resources/raw/branch/master/hardware/assets/prototype2.jpg)

The cobox logo is embossed onto the case and coloured by adding an Oracal HPC cut sticker. Adhestive rubber feet help protect the hard disk from vibrations, and honeycomb ventilation holes keep the CPU cool.

Small fibreglass strands are used to make the LEDs on the olimex visible on the enclosure.

The Olimex's battery connector is also exposed, for potential off-site use.

A complete breakdown of the hardware components, with costs and links to suppliers is available [here](https://ledger-git.dyne.org/CoBox/cobox-resources/src/commit/668ee5de28b1c8bdef52812fb544f03e74ae2aaa/hardware/enclosure-feature-and-parts-list.pdf)

And more in depth notes on our design process are available [here](https://ledger-git.dyne.org/CoBox/cobox-resources/src/branch/master/hardware/design-notes.md).
