#!/bin/bash
# Using image Armbian Bionic: Armbian_20.02.0-rc0_Lime2_bionic_current_5.4.12
sudo apt update && apt upgrade
sudo apt install python libtool
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.2/install.sh | bash
source ~/.bashrc
nvm install 11.15.0
npm i -g cobox-hub
# sudo apt install tmux
# npm i -g pm2
