
# Notes for cobox box design

![olimex lime2](https://www.olimex.com/Products/OLinuXino/A20/A20-OLinuXino-LIME2/images/thumbs/310x230/A20-OLinuXino-LIME2.jpg)

The device we are using is the [Olimex Lime2 ARM computer](https://www.olimex.com/Products/OLinuXino/A20/A20-OLinuXino-LIME2/open-source-hardware).

![board dimensions](https://www.olimex.com/Products/OLinuXino/A10/_resources/Ax0-OLINUXINO-LIME-dimensions.png)

[Circuit diagrams available here](https://github.com/OLIMEX/OLINUXINO/tree/master/HARDWARE/A20-OLinuXino-LIME2/1.%20Latest%20hardware%20revision)

The company that make it do [offer a case](https://www.olimex.com/Products/OLinuXino/BOXES/BOX-LIME) but our project needs also a hard-disk.  A 2.5 inch SATA disk, like the hard disk in most laptops.  The hard disk will need to be secured, ideally by screws on both sides.

![hard disk](./assets/hdd.jpg)

The Lime2 has connectors on 2 sides of the board. But luckily we only need the case to give access one side - we will need openings for the power cable, ethernet connector, and two usb ports, which are all on one side. 

Below you can see the original case offered by olimex, with a hard disk on top of it:

![side](./assets/side.jpg)

Here is the bottom of the case where you can see the screws with fix the board in place.

![bottom](./assets/bottom.jpg)

I think the simplest would be to laser cut acrylic, something like this:

![](./assets/tuxpi2.jpg)

Which would give a box looking something like this:

![](./assets/tuxpi1.jpg)

[makercase.com](https://www.makercase.com/) allows you to generate an SVG of a box to a custom size, with tenon joins. So we could do that and then use an SVG editor like inkscape to add the cutouts for the connectors. 

Ideally, we would like something made from recycled or natural materials. Nina suggested making a box out of mycelium grown in a mould.  Which would be amazing.  But given the time contraints, we maybe need to go for something simpler.

- [making a custom raspberry pi case](https://lowpowerlab.com/2015/01/22/making-a-custom-raspberrypi-case/)
- This article we found quite inspiring: [making enclosures for electronics](https://www.ponoko.com/blog/ponoko/making-enclosures-for-electronics-with-ponoko/)

If we do not want to build something from scratch there are companies which offer customisation of standard cases like this one:

![box](./assets/quote37.png)

This image is from [polycase.com](https://www.polycase.com/), who are based in the united states, but ideally we would like to find a supplier in europe.
