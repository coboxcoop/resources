
## Cobox hardware device components list

- [Olimex lime2](https://www.olimex.com/Products/OLinuXino/A20/A20-OLinuXino-LIME2/open-source-hardware) - €40
- [SATA Cable set](https://www.olimex.com/Products/Components/Cables/SATA-CABLE-SET/) - €5
- 5V Power supply [Chinese producer](https://www.olimex.com/Products/Power/SY0605E-CHINA/) - €3.95 [European producer](https://www.olimex.com/Products/Power/SY0605E/) - €6.95
- Hard disk, and SD card. Both of these can be bought from Olimex but will be cheaper elsewhere.  If buying a version of the Lime2 with on-board NAND memory you wont need an SD card, but it might be more difficult to flash an image onto it.  Both of these can also be easily found in old computers and phones, which quite possibly you already have lying around somewhere.  2GB is probably sufficient for the SD card, if also having a hard disk. With no harddisk, an SD card of at least 32GB would be sensible.
- Enclosure. Either the 3D printed one or [the stock case from Olimex](https://www.olimex.com/Products/OLinuXino/BOXES/BOX-LIME/) (€6) which does not fit the hard disk inside.
- You also need a micro SD card reader for your computer (or an adaptor for a normal sized SD card if you have a computer with an integrated SD card reader).
