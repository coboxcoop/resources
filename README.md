<div align="center">
  <img src="https://cobox.cloud/src/svg/logo.svg">
</div>

# CoBox Resources

This is the historical resources section of CoBox. To get the latest resources please go to our [wiki](https://gitlab.com/coboxcoop/readme/-/wikis/home).

## About

If you want to read more about CoBox you will find general information at [cobox.cloud](https://cobox.cloud). If you want to learn **how** CoBox works checkout out [docs.cobox.cloud](https://docs.cobox.cloud). If you want to learn about **why** we make CoBox or about the history of the project, research and milestones check the [wiki](https://gitlab.com/coboxcoop/readme/-/wikis/home). Lastly, if you are enjoying CoBox you might consider writing in our [guestbook](https://gitlab.com/coboxcoop/guestbook).

## Contributing

If you are a developer we suggest you start with the [mono-repo](https://gitlab.com/coboxcoop/mono) which exposes a [VueJS UI](https://gitlab.com/coboxcoop/ui), an [ExpressJS JSON API](https://gitlab.com/coboxcoop/server), and a Yargs CLI for interacting with CoBox ['spaces'](](https://gitlab.com/coboxcoop/cli)) and ['seeders'](https://gitlab.com/coboxcoop/seeder-cli). 
